

import time
import numpy as np
import torch

def ndi(params):
    """
    
    Nonlinear Dipole Inversion. Gradient Descent solver.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map, in radians.
    params.K: dipole kernel in the frequency space.
    Optional fields:
    params.alpha: regularization weight (use small values for stability).
    params.maxOuterIter: maximum number of iterations.
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data).
    params.tau: gradient descent rate.
    params.precond: preconditionate solution (for stability).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map, in radians.
    out.iter: number of iterations.
    out.time: total elapsed time (including pre-calculations).
    
    """
    start_time = time.time()
    # Required parameters
    kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha = torch.tensor(params.get("alpha", 1E-5), dtype=torch.float)
    tau = torch.tensor(params.get("tau", 2.0), dtype=torch.float)
    N = phase.shape
    num_iter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", np.ones(N)), dtype=torch.float32)
    weight = weight * weight
    
    isPrecond = params.get("isPrecond", False)
    
    if isPrecond:
        x = torch.tensor(params.get("precond", weight * phase), dtype=torch.float32)
    else:
        x = torch.zeros(N, dtype=torch.float64)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    kernel = kernel.to(device)
    phase = phase.to(device)
    x = x.to(device)
    alpha = alpha.to(device)
    tau = tau.to(device)
    weight = weight.to(device)
    
    Kt = torch.conj(kernel)
    regTime = time.time()
    for t in range(num_iter):
        # Update x: susceptibility estimate
        x_prev = x
        phix = susc2field(kernel, x)
        loss = susc2field(Kt, weight * torch.sin(phix - phase)) - alpha * x
        x = x_prev - tau * loss
        
        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
    
    end_time = time.time()
    regTime = end_time - regTime
    total_time = end_time - start_time
    
    print(f'Regularization Time: {regTime:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')
    
    out = {"x": x.cpu().numpy().astype(np.float32), "iter": t+1, "time": total_time}

    return out


def ndi_auto(params):
    """
    
    Nonlinear Dipole Inversion. Gradient Descent solver with an automatic stopping criterion.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map, in radians.
    params.K: dipole kernel in the frequency space.
    Optional fields:
    params.alpha: regularization weight (use small values for stability).
    params.maxOuterIter: maximum number of iterations.
    params.voxelSize: spatial resolution.
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data).
    params.tau: gradient descent rate.
    params.precond: preconditionate solution (for stability).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map, in radians.
    out.iter: number of iterations.
    out.time: total elapsed time (including pre-calculations).
    out.regTime: elapsed time (excluding pre-calculations).
    
    """
    start_time = time.time()
    # Required parameters
    kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha = torch.tensor(params.get("alpha", 1E-5), dtype=torch.float32)
    voxelSize = np.array(params.get("voxelSize", [[1,1,1]]))
    tau = torch.tensor(params.get("tau", 2.0), dtype=torch.float)
    N = phase.shape
    num_iter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", np.ones(N)), dtype=torch.float32)
    weight = weight * weight
    
    isPrecond = params.get("isPrecond", False)
    
    if isPrecond:
        x = torch.tensor(params.get("precond", weight * phase), dtype=torch.float32)
    else:
        x = torch.zeros(N, dtype=torch.float64)
    
    m1, m2 = create_freqmasks_auto(voxelSize, kernel)
    m1 = torch.tensor(m1, dtype=torch.float64)
    m2 = torch.tensor(m2, dtype=torch.float64)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    kernel = kernel.to(device)
    phase = phase.to(device)
    x = x.to(device)
    alpha = alpha.to(device)
    tau = tau.to(device)
    weight = weight.to(device)
    m1 = m1.to(device)
    m2 = m2.to(device)
    
    
    regTime = time.time()
    for t in range(num_iter):
        # Update x: susceptibility estimate
        x_prev = x
        phix = susc2field(kernel, x)
        loss = susc2field(torch.conj(kernel), weight * torch.sin(phix - phase)) - alpha * x
        x = x_prev - tau * loss
        
        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
        
        e1, e2 = compute_freqe_auto(x, m1, m2)
        
        if e1 > e2 and t > 3:
            print('Automatic stop reached!')
            break
    
    end_time = time.time()
    regTime = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {regTime:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')

    out = {"x": x.cpu().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    return out


def ndiCG(params):
    """
    
    Nonlinear Dipole Inversion. Conjugate Gradient Descent solver.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map, in radians.
    params.K: dipole kernel in the frequency space.
    Optional fields:
    params.alpha: regularization weight (use small values for stability).
    params.maxOuterIter: maximum number of iterations.
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data).
    params.precond: preconditionate solution (for stability).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map, in radians.
    out.iter: number of iterations.
    out.time: total elapsed time (including pre-calculations).
    out.regTime: elapsed time (excluding pre-calculations).
    
    """
    start_time = time.time()
    # Required parameters
    kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha = torch.tensor(params.get("alpha", 1E-6), dtype=torch.float)
    N = phase.shape
    num_iter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", torch.ones(N)), dtype=torch.float32)
    weight = weight * weight
    
    isPrecond = params.get("isPrecond", False)
    
    if isPrecond:
        x = torch.tensor(params.get("precond", weight * phase), dtype=torch.float32)
    else:
        x = torch.zeros(N, dtype=torch.float64)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    phase = phase.to(device)
    kernel = kernel.to(device)
    x = x.to(device)
    weight = weight.to(device)
    alpha = alpha.to(device)

    regTime = time.time()
    # Initiate with a gradient descent, with automatic step by line search.
    phix = susc2field(kernel, x)
    dx = -susc2field(torch.conj(kernel), weight * torch.sin(phix - phase)) - alpha * x

    B = dx * dx
    A = dx * (susc2field(torch.conj(kernel), weight * torch.sin(susc2field(kernel, dx))) + alpha * dx)
    tau = torch.sum(B) / (torch.sum(A) + 1e-8)

    x_prev = x
    x = x_prev + tau * dx
    x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
    print(f'Iter: 0   Update: {x_update:.6f}')
 
    s = dx
    # Start CG steps
    for t in range(num_iter):
        # update x: susceptibility estimate
        x_prev = x
        phix = susc2field(kernel, x)
        dx_prev = dx
        dx = -susc2field(torch.conj(kernel), weight * torch.sin(phix - phase)) - alpha * x
        
        betaPR = max(torch.sum(dx * (dx - dx_prev)) / (torch.sum(dx * dx) + 1e-8), 0)  # Automatic reset
        
        s = dx + betaPR * s

        B = s * dx
        A = s * (susc2field(torch.conj(kernel), weight * torch.sin(susc2field(kernel, s))) + alpha * s)
        tau = torch.sum(B) / (torch.sum(A) + 1e-8)

        x = x_prev + tau * s

        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
        
    end_time = time.time()
    regTime = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {regTime:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')
    
    out = {"x": x.cpu().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    
    return out


def ndiCG_auto(params):
    """
    
    Nonlinear Dipole Inversion. Conjugate Gradient Descent solver.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map, in radians.
    params.K: dipole kernel in the frequency space.
    Optional fields:
    params.alpha: regularization weight (use small values for stability).
    params.maxOuterIter: maximum number of iterations.
    params.voxelSize: spatial resolution.
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data).
    params.precond: preconditionate solution (for stability).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map, in radians.
    out.iter: number of iterations.
    out.time: total elapsed time (including pre-calculations).
    out.regTime: elapsed time (excluding pre-calculations).
    
    """
    start_time = time.time()
    # Required parameters
    kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha = torch.tensor(params.get("alpha", 1E-6), dtype=torch.float)
    voxelSize = np.array(params.get("voxelSize", [[1,1,1]]))
    N = phase.shape
    num_iter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", torch.ones(N)), dtype=torch.float32)
    weight = weight * weight
    
    isPrecond = params.get("isPrecond", False)
    
    if isPrecond:
        x = torch.tensor(params.get("precond", weight * phase), dtype=torch.float32)
    else:
        x = torch.zeros(N, dtype=torch.float64)
    
    m1, m2 = create_freqmasks_auto(voxelSize, kernel)
    m1 = torch.tensor(m1, dtype=torch.float64)
    m2 = torch.tensor(m2, dtype=torch.float64)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    phase = phase.to(device)
    kernel = kernel.to(device)
    x = x.to(device)
    weight = weight.to(device)
    alpha = alpha.to(device)
    m1 = m1.to(device)
    m2 = m2.to(device)

    regTime = time.time()
    # Initiate with a gradient descent, with automatic step by line search.
    phix = susc2field(kernel, x)
    dx = -susc2field(torch.conj(kernel), weight * torch.sin(phix - phase)) - alpha * x

    B = dx * dx
    A = dx * (susc2field(torch.conj(kernel), weight * torch.sin(susc2field(kernel, dx))) + alpha * dx)
    tau = torch.sum(B) / (torch.sum(A) + 1e-8)

    x_prev = x
    x = x_prev + tau * dx
    x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
    print(f'Iter: 0   Update: {x_update:.6f}')

    s = dx
    # Start CG steps
    for t in range(num_iter):
        # update x: susceptibility estimate
        x_prev = x
        phix = susc2field(kernel, x)
        dx_prev = dx
        dx = -susc2field(torch.conj(kernel), weight * torch.sin(phix - phase)) - alpha * x
        
        betaPR = max(torch.sum(dx * (dx - dx_prev)) / (torch.sum(dx * dx) + 1e-8), 0)  # Automatic reset
        
        s = dx + betaPR * s

        B = s * dx
        A = s * (susc2field(torch.conj(kernel), weight * torch.sin(susc2field(kernel, s))) + alpha * s)
        tau = torch.sum(B) / (torch.sum(A) + 1e-8)

        x = x_prev + tau * s

        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
        
        e1, e2 = compute_freqe_auto(x, m1, m2)
        
        if e1 > e2 and t > 3:
            print('Automatic stop reached!')
            break
        
    end_time = time.time()
    regTime = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {regTime:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')
    
    out = {"x": x.cpu().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    return out


def susc2field(D, X):
    """
    
    Susceptibility to Field calculation
    
    """
    phi = torch.real(torch.fft.ifftn(D * torch.fft.fftn(X)))
    
    return phi


def create_freqmasks_auto(spatial_res, kernel):
    """
    
    Calculates the masks that define Regions of Interest in the Fourier domain of
    reconstructed QSM images. These masks are defined by boundares that depend on:
    1) Dipole kernel coefficients.
    2) Absolute frequency radial range.
    
    Parameters:
    spatial_res: voxel size in mm.
    kernel:  dipole kernel matrix, as calculated by the dipole_kernel function.
    
    Output:
    m1, m2: binary masks defining two regions in the Fourier domain, for NDI stopping.
    
    """
    N = kernel.shape

    center = (1 + np.floor_divide(N, 2))

    rin = np.max(spatial_res) * 0.65 / 2  #Radial boundaries of the masks are defined as absolute.
    rout = np.max(spatial_res) * 0.95 / 2  #frequency values [1/mm]. Please modify if needed.

    kx = np.arange(1, N[0] + 1).astype(float)
    ky = np.arange(1, N[1] + 1).astype(float)
    kz = np.arange(1, N[2] + 1).astype(float)

    kx -= center[0]
    ky -= center[1]
    kz -= center[2]

    delta_kx = spatial_res[0,0] / N[0]
    delta_ky = spatial_res[0,1] / N[1]
    delta_kz = spatial_res[0,2] / N[2]

    kx *= delta_kx
    ky *= delta_ky
    kz *= delta_kz

    kx = kx[:, np.newaxis, np.newaxis]
    ky = ky[np.newaxis, :, np.newaxis]
    kz = kz[np.newaxis, np.newaxis, :]

    kx = np.tile(kx, (1, N[1], N[2]))
    ky = np.tile(ky, (N[0], 1, N[2]))
    kz = np.tile(kz, (N[0], N[1], 1))

    k2 = kx ** 2 + ky ** 2 + kz ** 2

    m0 = np.ones(N)
    m0[k2 > rout ** 2] = 0.0
    m0[k2 < rin ** 2] = 0.0
    m0 = np.fft.fftshift(m0)  # Internal mask defining the radial range.

    kernel = kernel.cpu().numpy().astype(np.float32)

    m1 = m0 * (np.logical_xor(np.abs(kernel) > 0.15, np.abs(kernel) > 0.2))  # green
    m2 = m0 * (np.logical_xor(np.abs(kernel) > 0.225, np.abs(kernel) > 0.275))  # cyan

    return m1, m2


def compute_freqe_auto(chi, m1, m2):
    """
    
    Calculates the Mean Amplitude of the energy inside masks
    m1, m2, and (optional) m3 in the Fourier domain of QSM reconstruction chi.

    The estimation of the Mean Amplitude may be the mean magnitude
    of the coefficients (power = 1) or the squared mean of the
    coefficients (power = 2). Both are robust estimators.

    """
    fchi = torch.abs(torch.fft.fftn(chi))

    n1 = torch.sum(m1)
    n2 = torch.sum(m2)
    
    e1 = torch.sum(fchi * m1) / n1
    e2 = torch.sum(fchi * m2) / n2

    return e1, e2