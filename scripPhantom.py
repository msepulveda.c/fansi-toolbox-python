

import numpy as np
import scipy.io as sio
import utils_module
import eNDI_module_torch
import FANSI_module_torch
#import FANSI_module_cupy

def scripPhantom():
    """
    
    This is a sample script to show how to use the functions in this toolbox, and how to set the principal variables.
    This example uses an analytic brain phantom.
    
    Description:
    
    Nonlinear/Linear QSM with Total Variation and Total Generalized Variation regularization.
    Gradient Descent and Conjugate Gradient method to perform nonregularized Nonlinear Dipole Inversions.
    
    Requirements:
    Numpy
    PyTorch
    Scipy
    Matplotlib
    time
 
    """
    
    chiData = sio.loadmat("dataPhantom/chi_phantom.mat")
    maskUseData = sio.loadmat("dataPhantom/mask_phantom.mat")
    spatialResData = sio.loadmat("dataPhantom/spatial_res_phantom.mat")
    
    chi = chiData["chi"].astype(np.float32)
    maskUse = maskUseData["mask_use"]
    spatialRes = spatialResData["spatial_res"].astype(np.float64)

    N = np.shape(chi)
    pos = tuple(x // 2 for x in N)
   
    utils_module.imagesc3d2(chi - (maskUse == 0), pos, fig_num=1, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig="True Susceptibility")

    #Simulate magnitude data based on the susceptibility map   
    mag = chi - np.min(chi)
    mag = mag / np.max(mag)* maskUse
    
    kernel = utils_module.dipole_kernel(N, spatialRes)

    chi = chi - np.mean(chi)  #  Demean the susceptibility ground-truth
    phase_true = np.real(np.fft.ifftn(kernel * np.fft.fftn(chi)))  #  field map in ppm

    #Simulate acquisition parameters
    TE = 5e-3  #5ms
    B0 = 3
    gyro = 2 * np.pi * 42.58  #ppm-to-radian scaling factor
    phs_scale = TE *gyro * B0

    #Add noise
    SNR = 345  #peak SNR value
    noise = 1 / SNR
    signal = mag * np.exp(1j * phase_true * phs_scale) + noise * (np.random.randn(*N) + 1j * np.random.randn(*N))
    phase_use = np.angle(signal, deg=False) / phs_scale  # Let's work in ppm in this example.
                                              # Since we are simulating only a local field, and short TE,
                                              # the unwrapping step is skipped.
    magn_use = np.abs(signal) * maskUse
    rmse_noise = utils_module.compute_rmse(maskUse * phase_use, maskUse * phase_true)

    utils_module.imagesc3d2(phase_use, pos, fig_num=2, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"Noise RMSE: {rmse_noise:.4f}")
    utils_module.imagesc3d2(magn_use, pos, fig_num=3, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, contrast=[0,1],title_fig="Noisy Magnitude")
    
    """
    Nonlinear Dipole Inversion. Gradient Descent solver.
    """
    # Required parameters
    params = {}
    params["K"] = kernel
    params["input"] = phase_use
    params["alpha"] = 2e-4  # gradient L1 penalty
    # Optional
    params["weight"] = np.float32(maskUse)  # To avoid noise from external regions to corrupt the solution,
                                            # it is recommended to either mask this weight, or the input data.
    params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    
    
    out = eNDI_module_torch.ndi(params)
    #out = eNDI_module_torch.ndi_auto(params)
    rmse_ndi = utils_module.compute_rmse(maskUse * out["x"], chi)
    
    params["alpha"] = 1e-4
    params["weight"] = np.float32(magn_use)  # Compare it with a reconstruction using the magnitude as data fidelity weight.
                                         # Note that the magnitude is normalized to the 0-1 range.
    outw = eNDI_module_torch.ndi(params)
    #outw = eNDI_module_torch.ndi_auto(params)
    rmse_ndiw = utils_module.compute_rmse(maskUse * outw["x"], chi)
    
    # Display results
    utils_module.imagesc3d2(out["x"] * maskUse - (maskUse == 0), pos, fig_num=4, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"NDI RMSE: {rmse_ndi:.4f}")
    utils_module.imagesc3d2(outw["x"] * maskUse - (maskUse == 0), pos, fig_num=5, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"Weighted NDI RMSE: {rmse_ndiw:.4f}")
    
    """
    Nonlinear Dipole Inversion. Conjugate Gradient Descent solver.
    """
    # Required parameters
    params = {}  # reset the structure
    params["K"] = kernel
    params["input"] = phase_use
    params["alpha"] = 2e-4  # gradient L1 penalty
    # Optional
    params["weight"] = np.float32(maskUse)  # To avoid noise from external regions to corrupt the solution,
                                            # it is recommended to either mask this weight, or the input data.
    params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    
    out2 = eNDI_module_torch.ndiCG(params)
    #out2 = eNDI_module_torch.ndiCG_auto(params)
    rmse_ndiCG = utils_module.compute_rmse(maskUse * out2["x"], chi)
        
    params["alpha"] = 1e-4
    params["weight"] = np.float32(magn_use)  # Compare it with a reconstruction using the magnitude as data fidelity weight.
        
    out2w = eNDI_module_torch.ndiCG(params)
    #out2w = eNDI_module_torch.ndiCG_auto(params)
    rmse_ndiCGw = utils_module.compute_rmse(maskUse * out2w["x"], chi)
    
    # Display results
    utils_module.imagesc3d2(out2["x"] * maskUse - (maskUse == 0), pos, fig_num=6, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"NDI CG RMSE: {rmse_ndiCG:.4f}")
    utils_module.imagesc3d2(out2w["x"] * maskUse - (maskUse == 0), pos, fig_num=7, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"Weighted NDI CG RMSE: {rmse_ndiCGw:.4f}")
    
    """
    Linear QSM and Total Variation regularization.
    """
    # Required parameters
    params = {}  # reset the structure
    params["K"] = kernel
    params["input"] = phase_use
    params["alpha"] = 2e-4  # gradient L1 penalty
    params["isNonlinear"] = False
    
    # Optional
    params["weight"] = np.float32(maskUse)  # To avoid noise from external regions to corrupt the solution,
                                            # it is recommended to either mask this weight, or the input data.
    #params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    out3 = FANSI_module_torch.fansiTV(params)
    #out3 = FANSI_module_cupy.fansiTV(params)
    rmse_tv = utils_module.compute_rmse(maskUse * out3["x"], chi)
    
    params["alpha"] = 1e-4
    params["weight"] = np.float32(magn_use)  # Compare it with a reconstruction using the magnitude as data fidelity weight.
    
    out3w = FANSI_module_torch.fansiTV(params)
    #out3w = FANSI_module_cupy.fansiTV(params)
    rmse_tvw = utils_module.compute_rmse(maskUse * out3w["x"], chi)
    
    # Display results
    utils_module.imagesc3d2(out3["x"] * maskUse - (maskUse == 0), pos, fig_num=8, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"FANSI TV RMSE: {rmse_tv:.4f}")
    utils_module.imagesc3d2(out3w["x"] * maskUse - (maskUse == 0), pos, fig_num=9, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"Weighted TV RMSE: {rmse_tvw:.4f}")
    
    """
    Nonlinear QSM and Total Variation regularization.
    """
    
    # Required parameters
    params = {}  # reset the structure
    params["K"] = kernel
    params["input"] = phase_use*phs_scale
    params["alpha"] = 2e-4  # gradient L1 penalty
    params["isNonlinear"] = True
    # Optional
    params["weight"] = np.float32(magn_use)  # % Recommended for nonlinear algorithms.
    #params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    
    out4 = FANSI_module_torch.fansiTV(params)
    #out4 = FANSI_module_cupy.fansiTV(params)
    rmse_nltv = utils_module.compute_rmse(out4["x"] * maskUse/phs_scale, chi)
    
    # Display results
    utils_module.imagesc3d2(out4["x"] * maskUse/phs_scale - (maskUse == 0), pos, fig_num=10, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"FANSI nlTV RMSE: {rmse_nltv:.4f}")
    
    """
    Linear QSM and Total Generalized Variation regularization.
    """
    # Required parameters
    params = {}  # reset the structure
    params["K"] = kernel
    params["input"] = phase_use
    params["alpha"] = 2e-4  # gradient L1 penalty
    params["isNonlinear"] = False
    
    # Optional
    params["weight"] = np.float32(maskUse)  # To avoid noise from external regions to corrupt the solution,
                                            # it is recommended to either mask this weight, or the input data.
    #params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    out5 = FANSI_module_torch.fansiTGV(params)
    #out5 = FANSI_module_cupy.fansiTGV(params)
    rmse_tgv = utils_module.compute_rmse(maskUse * out5["x"], chi)
    
    params["alpha"] = 1e-4
    params["weight"] = np.float32(magn_use)  # Compare it with a reconstruction using the magnitude as data fidelity weight.
    
    out5w = FANSI_module_torch.fansiTGV(params)
    #out5w = FANSI_module_cupy.fansiTGV(params)
    rmse_tgvw = utils_module.compute_rmse(maskUse * out5w["x"], chi)
    
    # Display results
    utils_module.imagesc3d2(out5["x"] * maskUse - (maskUse == 0), pos, fig_num=11, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"FANSI TGV RMSE: {rmse_tgv:.4f}")
    utils_module.imagesc3d2(out5w["x"] * maskUse - (maskUse == 0), pos, fig_num=12, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"Weighted TGV RMSE: {rmse_tgvw:.4f}")
    
    """
    Nonlinear QSM and Total Generalized Variation regularization.
    """
    
    # Required parameters
    params = {}  # reset the structure
    params["K"] = kernel
    params["input"] = phase_use*phs_scale
    params["alpha"] = 2e-4  # gradient L1 penalty
    params["isNonlinear"] = True
    # Optional
    params["weight"] = np.float32(magn_use)  # % Recommended for nonlinear algorithms.
    #params["maxOuterIter"] = 50  # Use these values for faster reconstructions.
    
    out6 = FANSI_module_torch.fansiTGV(params)
    #out6 = FANSI_module_cupy.fansiTGV(params)
    rmse_nltgv = utils_module.compute_rmse(out6["x"] * maskUse/phs_scale, chi)
    
    # Display results
    utils_module.imagesc3d2(out6["x"] * maskUse/phs_scale - (maskUse == 0), pos, fig_num=13, rot_deg=[90,90,90], scale_fig=[-0.12,0.12, -0.12, 0.12], avg_size=0, title_fig=f"FANSI nlTGV RMSE: {rmse_nltgv:.4f}")

    
if __name__ == "__main__":

    scripPhantom()
