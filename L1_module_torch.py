

import time
import numpy as np
import torch
import torch.fft as fft

def L1_TV(params):
    """
    
    Nonlinear/Linear L1-norm QSM and Total Variation regularization with
    spatially variable fidelity and regularization weights.
    
    The L1-norm data fidelity term (PI-QSM) is more robust against phase
    inconsistencies than standard L2-norm (FANSI). 
    
    This uses ADMM to solve the functional. 
       
    Parameters: params - structure with 
    Required fields:
    params.input: local field map.
    params.K: dipole kernel in the frequency space.
    params.alpha1: gradient penalty (L1-norm) or regularization weight.
    Optional fields:
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1).
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0).
    params.maxOuterIter: maximum number of iterations (recommended = 150).
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1).
    params.tolDelta: (recommended = 1e-6).
    params.weight: data fidelity spatially variable weight (recommended = lambda*magnitude_data
                                                            or lambda*mask), with the magnitude in the [0,1] range.
                    IMPORTANT: This parameter affects the L1 proximal operation performed
                    between the predicted magnetization and the acquired data. Unlike FANSI,
                    this weight should be rescaled by a lambda factor to increase or decrease
                    the phase rejection strength. Lambda < 1 rejects more voxels with measured inconsistencies.                 
    params.regWeight: regularization spatially variable weight.
    params.isNonlinear: Linear or nonlinear algorithm?(default = true).
    params.voxelSize: voxel size (default value = [1,1,1]).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map.
    out.iter: number of iterations needed.
    out.time: total elapsed time (including pre-calculations).
    
    """
    
    start_time = time.time()
    # Required parameters
    Kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha1 = torch.tensor(params.get("alpha", 1E-5), dtype=torch.float)
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", np.ones(N)), dtype=torch.float32)
    W = weight 
    mu1 = params.get("mu1", 100 * alpha1).clone().detach().requires_grad_(False).to(torch.float32)
    mu2 = torch.tensor(params.get("mu2", 1), dtype=torch.float32)
    mu3 = torch.tensor(params.get("mu3", 1), dtype=torch.float32)
    tolUpdate = torch.tensor(params.get("tolUpdate", 0.1))
    voxelSize = np.array(params.get("voxelSize", [[1,1,1]]))
    regWeight = torch.from_numpy(np.ones(N + (3,)))
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = torch.tensor(params.get("tolDelta", 1E-6))
        IS = torch.exp(1j * phase)
        s3 = torch.zeros(N, dtype=torch.float64)
        z2 =  W * phase / torch.max(W)
    else:
        z2 =  W * phase

    # Variable initialization
    z_dx = torch.zeros(N, dtype=torch.float32)
    z_dy = torch.zeros(N, dtype=torch.float32)
    z_dz = torch.zeros(N, dtype=torch.float32)

    s_dx = torch.zeros(N, dtype=torch.float32)
    s_dy = torch.zeros(N, dtype=torch.float32)
    s_dz = torch.zeros(N, dtype=torch.float32)


    x = torch.zeros(N, dtype=torch.float32)
    s2 = torch.zeros(N, dtype=torch.float32)
    alpha_over_mu = (alpha1 / mu1).clone().detach().requires_grad_(False).to(torch.float32) # for efficiency

    # Define the operators
    k1, k2, k3 = torch.meshgrid(torch.arange(N[0]), torch.arange(N[1]), torch.arange(N[2]), indexing='xy')

    E1 = (1 - torch.exp(2j * torch.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - torch.exp(2j * torch.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - torch.exp(2j * torch.pi * k3 / N[2])) / voxelSize[0, 2]

    zero = torch.tensor(0) 
    
    torch.cuda.empty_cache()
    # Move variables to GPU if available  
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if isNonlinear:
        IS = IS.to(device)
        s3 = s3.to(device)
        mu3 = mu3.to(device)
        zero_5 = torch.tensor(0.05).to(device)

    else:
        phase = phase.to(device)
    
    z_dx = z_dx.to(device)
    z_dy = z_dy.to(device)
    z_dz = z_dz.to(device)
    
    s_dx = s_dx.to(device)
    s_dy = s_dy.to(device)
    s_dz = s_dz.to(device)
    
    x = x.to(device)
    Kernel = Kernel.to(device)
    
    z2 = z2.to(device)
    s2 = s2.to(device)
    
    tolUpdate = tolUpdate.to(device)
    
    E1 = E1.to(device)
    E2 = E2.to(device)
    E3 = E3.to(device)
    
    alpha_over_mu = alpha_over_mu.to(device)
    regWeight = regWeight.to(device)
    W = W.to(device)
    mu1 = mu1.to(device)
    mu2 = mu2.to(device)
    zero = zero.to(device)


    E1t = torch.conj(E1)
    E2t = torch.conj(E2)
    E3t = torch.conj(E3)

    EE2 = E1t * E1 + E2t * E2 + E3t * E3
    
    
    Kt = torch.conj(Kernel)
    regTime = time.time()
    
    for t in range(maxOuterIter):
        # Update x: susceptibility estimate
        tx = E1t * fft.fftn(z_dx - s_dx)
        ty = E2t * fft.fftn(z_dy - s_dy)
        tz = E3t * fft.fftn(z_dz - s_dz)

        x_prev = x.clone()
        if isNonlinear:
            x = torch.real(fft.ifftn((mu1 * (tx + ty + tz) + mu2 * Kt * fft.fftn(z2 - s2)) / (torch.finfo(torch.float64).eps + mu2 * torch.abs(Kernel) ** 2 + mu1 * EE2) ));
        else:
            x = torch.real(fft.ifftn((mu1 * (tx + ty + tz) + mu2 * Kt * fft.fftn(z2 - s2 + phase)) / (torch.finfo(torch.float64).eps + mu2 * torch.abs(Kernel) ** 2 + mu1 * EE2)));
          
        del tx
        del ty
        del tz 
            
        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
            
        if x_update < tolUpdate or torch.isnan(x_update):
            print("\nEarly stopping reached.")
            break

        if t < maxOuterIter:
            # Update z: gradient variable
            Fx = fft.fftn(x)
            x_dx = torch.real(fft.ifftn(E1 * Fx))
            x_dy = torch.real(fft.ifftn(E2 * Fx))
            x_dz = torch.real(fft.ifftn(E3 * Fx))

            z_dx = torch.maximum(torch.abs(x_dx + s_dx) - regWeight[:, :, :, 0] * alpha_over_mu, zero) * torch.sign(x_dx + s_dx)
            z_dy = torch.maximum(torch.abs(x_dy + s_dy) - regWeight[:, :, :, 1] * alpha_over_mu, zero) * torch.sign(x_dy + s_dy)
            z_dz = torch.maximum(torch.abs(x_dz + s_dz) - regWeight[:, :, :, 2] * alpha_over_mu, zero) * torch.sign(x_dz + s_dz)

            # Update s: Lagrange multiplier
            s_dx = s_dx + x_dx - z_dx
            s_dy = s_dy + x_dy - z_dy
            s_dz = s_dz + x_dz - z_dz

            del x_dx
            del x_dy
            del x_dz                

            # Update z2 and s2: data consistency
            
            if isNonlinear:
                Y3 = torch.exp(1j * z2) - IS + s3  #  aux variable
                z3 = torch.max(torch.abs(Y3) - W / (mu3+torch.finfo(torch.float64).eps), zero) * torch.sgn(Y3)  # proximal operation
                del Y3
                rhs_z2 = mu2 * torch.real(fft.ifftn(Kernel * Fx) + s2)
                z2 = rhs_z2 / mu2
                
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                yphase = torch.angle (IS + z3 - s3)
                ym = torch.abs(IS + z3 - s3)
                while (delta > tolDelta and inn < 4):
                    inn = inn + 1
                    norm_old = torch.norm(z2.reshape(-1))
                    
                    temp = mu3 * torch.cos(z2 - yphase - 1j * torch.log(ym)) + mu2 + torch.finfo(torch.float64).eps
                    update = torch.real(( mu3 * torch.sin(z2 - yphase - 1j * torch.log(ym)) + mu2 * z2 - rhs_z2) / (torch.max(torch.abs(temp), zero_5) * torch.sgn(temp)))
                
                    z2 = z2 - update
                    delta_new = torch.norm(update.reshape(-1)) / norm_old             
                    if delta_new > delta:
                        break
                    delta = delta_new
                    
                del update
                del rhs_z2
                del yphase
                del ym
                del temp
                
                s2 = s2 + torch.real(fft.ifftn(Kernel * Fx)) - z2
                s3 = torch.exp(1j * z2) - IS + s3 - z3
                
                del Fx
            else:
                z2_inner = torch.real(fft.ifftn(Kernel * Fx)) + s2 - phase
                z2 = torch.max(torch.abs(z2_inner) - W / mu2, zero) * torch.sign(z2_inner)  # proximal operation
        
                s2 = z2_inner - z2
                del Fx
                del z2_inner
                     
          
    print('\n')
    
    # Extract output values
    end_time = time.time()
    reg_time = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')

    out = {"x": x.cpu().detach().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    
    return out
