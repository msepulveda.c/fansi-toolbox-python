

import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import rotate

def dipole_kernel(N, spatial_res):
    """
    
    This function calculates the dipole kernel used in QSM (single orientation)
    that models the susceptibility-to-field convolution.
    Use this function if the main field direction is along the z axis.
    In addition to the standard dipole kernel function, additional approximations are provided.
    Continuous kernel proposed by Salomir, et al. 2003.
    
    Parameters:
    N: array size.
    spatial_res: voxel size in mm.
    
    Output:
    kernel: dipole kernel in the frequency space.
    
    """
    ky, kx, kz = np.meshgrid(np.arange(-np.floor(N[1]/2), np.ceil(N[1]/2)), 
                            np.arange(-np.floor(N[0]/2), np.ceil(N[0]/2)), 
                            np.arange(-np.floor(N[2]/2), np.ceil(N[2]/2)))

    kx = (kx.astype(np.float32) / np.max(np.abs(kx.astype(np.float32)))) / spatial_res[0, 0]
    ky = (ky.astype(np.float32) / np.max(np.abs(ky.astype(np.float32)))) / spatial_res[0, 1]
    kz = (kz.astype(np.float32) / np.max(np.abs(kz.astype(np.float32)))) / spatial_res[0, 2]


    k2 = kx**2 + ky**2 + kz**2
    k2[k2 == 0] = np.finfo(float).eps

    kernel = np.fft.ifftshift(1/3 - (kz**2) / k2)  # Keep the center of the frequency domain at [1,1,1]
    kernel[0, 0, 0] = 0.0

    return kernel


def imagesc3d2(img, pos=None, fig_num=1, rot_deg=[0, 0, 0], scale_fig=None, contrast = [-0.1, 0.1], avg_size=0, title_fig=" "):
    """

    Display different views of the 3D data.

    Parameters:
    img: 3D data to be displayed.
    pos: vector defining the point at the center of the three projection planes.
    fig_num: figure number.
    rot_deg: rotation of each subfigure.
    scale_fig: data range to be displayed (black and white points).
    avg_size: downsample factor in voxels (0 to avoid subsampling).
    title_fig: title.
    
    Example:
    imagesc3d2(chi_cosmos, pos=size(chi_cosmos) // 2, fig_num=1, rot_deg=[90, 90, -90], scale_fig=[-0.10, 0.14], title_fig='COSMOS')
    
    """
    if np.linalg.norm(np.imag(img)) > 0:
        img = np.abs(img)

    if pos is None:

        pos = np.round(np.array(img.shape) / 2).astype(int)

    if scale_fig is None:
        scale_fig = [np.min(img), np.max(img), np.min(img), np.max(img)]
    
    def compute_avg_size(avg_size):
        if avg_size > 0:
            if avg_size % 2:
                Lavg_size = (avg_size - 1) // 2
                Ravg_size = (avg_size - 1) // 2
            else:
                Lavg_size = (avg_size // 2) - 1
                Ravg_size = avg_size // 2
            return Lavg_size, Ravg_size
        return 0, 0
    
    Lavg_size, Ravg_size = compute_avg_size(avg_size)

    # Obtain the sections of the img matrix
    section_x = np.squeeze(np.mean(img[pos[0] - Lavg_size:pos[0] + 1 + Ravg_size, :, :], axis=0))
    section_y = np.squeeze(np.mean(img[:, pos[1]- Lavg_size:pos[1] + 1 + Ravg_size, :], axis=1))
    section_z = np.mean(img[:, :, pos[2]- Lavg_size:pos[2] + 1 + Ravg_size], axis=2)

    # Add padding so that the sections have the same dimensions
    max_dim = max(section_x.shape[0], section_x.shape[1], section_y.shape[0], section_y.shape[1])
    section_x = np.pad(section_x, ((0, max_dim - section_x.shape[0]), (0, max_dim - section_x.shape[1])), mode='constant', constant_values=0)
    section_y = np.pad(section_y, ((0, max_dim - section_y.shape[0]), (0, max_dim - section_y.shape[1])), mode='constant', constant_values=0)
    
    # Apply rotation to the sections
    rotated_section_x = rotate(section_x, angle=rot_deg[0], reshape=False)
    rotated_section_y = rotate(section_y, angle=rot_deg[1], reshape=False)
    rotated_section_z = rotate(section_z, angle=rot_deg[2], reshape=False)
    
    # Create a figure with three subplots
    plt.figure(1, figsize=(12, 4), dpi=200, facecolor='black')
            
    plt.subplot(131)
    plt.imshow(rotated_section_x, cmap='gray', vmin = contrast[0], vmax = contrast[1], extent=scale_fig, origin='upper')
    plt.axis('image')
    plt.xlim(-0.12, 0.12)
    plt.ylim(-0.12, -0.03)
    plt.axis('off')
            
    plt.subplot(132)
    plt.imshow(rotated_section_y, cmap='gray', vmin = contrast[0], vmax = contrast[1], extent=scale_fig, origin='upper')
    plt.axis('image')
    plt.xlim(-0.12, 0.12)
    plt.ylim(-0.12, -0.03)
    plt.axis('off')
    plt.title(title_fig, color='w', fontsize=24)
    
    plt.subplot(133)
    plt.imshow(rotated_section_z, cmap='gray', vmin = contrast[0], vmax = contrast[1], extent=scale_fig, origin='upper')
    plt.axis('image')
    plt.axis('off')

                
    plt.tight_layout()
    plt.show()
    
    
def compute_rmse(chi_recon, chi_true):
    """
    
    Calculates the L2 norm of the difference between two images.
    
    """
    rmse = 100 * np.linalg.norm(chi_recon - chi_true) / np.linalg.norm(chi_true)
    
    return rmse