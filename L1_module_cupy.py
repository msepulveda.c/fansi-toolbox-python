

import time
import numpy as np
try:
    import cupy as cp
    num_gpus = cp.cuda.runtime.getDeviceCount()
    if num_gpus > 0:
        gpu_available = True
    else:
        gpu_available = False
except (ImportError, cp.cuda.runtime.CUDARuntimeError):
    cp = None
    gpu_available = False

def L1_TV(params):
    """
    
    Nonlinear/Linear L1-norm QSM and Total Variation regularization with
    spatially variable fidelity and regularization weights.
    
    The L1-norm data fidelity term (PI-QSM) is more robust against phase
    inconsistencies than standard L2-norm (FANSI). 
    
    This uses ADMM to solve the functional. 
       
    Parameters: params - structure with 
    Required fields:
    params.input: local field map.
    params.K: dipole kernel in the frequency space.
    params.alpha1: gradient penalty (L1-norm) or regularization weight.
    Optional fields:
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1).
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0).
    params.maxOuterIter: maximum number of iterations (recommended = 150).
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1).
    params.tolDelta: (recommended = 1e-6).
    params.weight: data fidelity spatially variable weight (recommended = lambda*magnitude_data
                                                            or lambda*mask), with the magnitude in the [0,1] range.
                    IMPORTANT: This parameter affects the L1 proximal operation performed
                    between the predicted magnetization and the acquired data. Unlike FANSI,
                    this weight should be rescaled by a lambda factor to increase or decrease
                    the phase rejection strength. Lambda < 1 rejects more voxels with measured inconsistencies.                 
    params.regWeight: regularization spatially variable weight.
    params.isGPU: GPU acceleration (default = true).
    params.isNonlinear: Linear or nonlinear algorithm?(default = true).
    params.voxelSize: voxel size (default value = [1,1,1]).
    
    Output: out - structure with the following fields:
    out.x: calculated susceptibility map.
    out.iter: number of iterations needed.
    out.time: total elapsed time (including pre-calculations).
    
    """ 
    
    start_time = time.time()
    
    isGPU = params.get("isGPU", True)
    
    if isGPU and gpu_available:
        xp = cp
        print("GPU")
    else:
        xp = np
        print("CPU")
        
    # Required parameters
    Kernel = xp.array(params["K"], dtype=xp.float32)
    phase = xp.array(params["input"], dtype=xp.float64)

    alpha1 = xp.array(params.get("alpha", 1E-5), dtype=xp.float32)
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)
    
    weight = xp.array(params.get("weight", xp.ones(N)), dtype=xp.float32)
    W = weight 
    mu1 = params.get("mu1", 100 * alpha1).astype(xp.float32)
    mu2 = xp.array(params.get("mu2", 1), dtype=xp.float32)
    mu3 = xp.array(params.get("mu3", 1), dtype=xp.float32)
    
    tolUpdate = xp.array(params.get("tolUpdate", 0.1))
    voxelSize = xp.array(params.get("voxelSize", [[1, 1, 1]]))
    regWeight = xp.ones(N + (3,), dtype=xp.float32)
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = xp.array(params.get("tolDelta", 1E-6))
        IS = xp.exp(1j * phase)
        s3 = xp.zeros(N, dtype=xp.float64)
        z2 =  W * phase / xp.max(W)
    else:
        z2 =  W * phase

    # Variable initialization
    z_dx = xp.zeros(N, dtype=xp.float32)
    z_dy = xp.zeros(N, dtype=xp.float32)
    z_dz = xp.zeros(N, dtype=xp.float32)

    s_dx = xp.zeros(N, dtype=xp.float32)
    s_dy = xp.zeros(N, dtype=xp.float32)
    s_dz = xp.zeros(N, dtype=xp.float32)

    x = xp.zeros(N, dtype=xp.float32)
    s2 = xp.zeros(N, dtype=xp.float32)
    alpha_over_mu = (alpha1 / mu1).astype(xp.float32)  # for efficiency
    
    # Define the operators
    k1, k2, k3 = xp.meshgrid(xp.arange(N[0]), xp.arange(N[1]), xp.arange(N[2]), indexing='xy')

    E1 = (1 - xp.exp(2j * xp.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - xp.exp(2j * xp.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - xp.exp(2j * xp.pi * k3 / N[2])) / voxelSize[0, 2]

    E1t = xp.conj(E1)
    E2t = xp.conj(E2)
    E3t = xp.conj(E3)
    Kt = xp.conj(Kernel)
    
    EE2 = E1t * E1 + E2t * E2 + E3t * E3
    
    
    
    regTime = time.time()   
    for t in range(maxOuterIter):
        # Update x: susceptibility estimate
        tx = E1t * xp.fft.fftn(z_dx - s_dx)
        ty = E2t * xp.fft.fftn(z_dy - s_dy)
        tz = E3t * xp.fft.fftn(z_dz - s_dz)

        x_prev = x
        if isNonlinear:
            x = xp.real(xp.fft.ifftn((mu1 * (tx + ty + tz) + mu2 * Kt * xp.fft.fftn(z2 - s2)) / (xp.finfo(xp.float64).eps + mu2 * xp.abs(Kernel) ** 2 + mu1 * EE2) ));
        else:
            x = xp.real(xp.fft.ifftn((mu1 * (tx + ty + tz) + mu2 * Kt * xp.fft.fftn(z2 - s2 + phase)) / (xp.finfo(xp.float64).eps + mu2 * xp.abs(Kernel) ** 2 + mu1 * EE2)));
          
        del tx
        del ty
        del tz 
            
        x_update = 100 * xp.linalg.norm(x - x_prev) / xp.linalg.norm(x)
        
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
            
        if x_update < tolUpdate or xp.isnan(x_update):
            print("\nEarly stopping reached.")
            break

        if t < maxOuterIter:
            # Update z: gradient variable
            Fx = xp.fft.fftn(x)
            x_dx = xp.real(xp.fft.ifftn(E1 * Fx))
            x_dy = xp.real(xp.fft.ifftn(E2 * Fx))
            x_dz = xp.real(xp.fft.ifftn(E3 * Fx))

            z_dx = xp.maximum(xp.abs(x_dx + s_dx) - regWeight[:, :, :, 0] * alpha_over_mu, 0) * xp.sign(x_dx + s_dx)
            z_dy = xp.maximum(xp.abs(x_dy + s_dy) - regWeight[:, :, :, 1] * alpha_over_mu, 0) * xp.sign(x_dy + s_dy)
            z_dz = xp.maximum(xp.abs(x_dz + s_dz) - regWeight[:, :, :, 2] * alpha_over_mu, 0) * xp.sign(x_dz + s_dz)

            # Update s: Lagrange multiplier
            s_dx = s_dx + x_dx - z_dx
            s_dy = s_dy + x_dy - z_dy
            s_dz = s_dz + x_dz - z_dz

            del x_dx
            del x_dy
            del x_dz                

            # Update z2 and s2: data consistency
            
            if isNonlinear:
                Y3 = xp.exp(1j * z2) - IS + s3  #  aux variable
                z3 = xp.maximum(xp.abs(Y3) - W / (mu3 + xp.finfo(xp.float64).eps), 0) * (Y3/(xp.abs(Y3)+xp.finfo(xp.float64).eps))  # proximal operation
                del Y3
                rhs_z2 = mu2 * xp.real(xp.fft.ifftn(Kernel * Fx) + s2)
                z2 = rhs_z2 / mu2
                
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                yphase = xp.angle(IS + z3 - s3)
                ym = xp.abs(IS + z3 - s3)
                while (delta > tolDelta and inn < 4):
                    inn = inn + 1
                    norm_old = xp.linalg.norm(z2.reshape(-1))
                    
                    temp = mu3 * xp.cos(z2 - yphase - 1j * xp.log(ym)) + mu2 + xp.finfo(xp.float64).eps
                    update = xp.real((mu3 * xp.sin(z2 - yphase - 1j * xp.log(ym)) + mu2 * z2 - rhs_z2) / (xp.maximum(xp.abs(temp), 0.05) * (temp/(xp.abs(temp)+xp.finfo(xp.float64).eps)) ))
                
                    z2 = z2 - update
                    delta_new = xp.linalg.norm(update.reshape(-1)) / norm_old             
                    if delta_new > delta:
                        break
                    delta = delta_new
                    
                del update
                del rhs_z2
                del yphase
                del ym
                del temp
                
                s2 = s2 + xp.real(xp.fft.ifftn(Kernel * Fx)) - z2
                s3 = xp.exp(1j * z2) - IS + s3 - z3
                
                del Fx
            else:
                z2_inner = xp.real(xp.fft.ifftn(Kernel * Fx)) + s2 - phase
                z2 = xp.maximum(xp.abs(z2_inner) - W / mu2, 0) * xp.sign(z2_inner)  # proximal operation
        
                s2 = z2_inner - z2
                del Fx
                del z2_inner
                     
          
    print('\n')
    
    # Extract output values
    end_time = time.time()
    reg_time = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')

    if isGPU and gpu_available:
        out = {"x": cp.asnumpy(x).astype(np.float32), "iter": t + 1, "time": total_time}
    else:
        out = {"x": x.astype(np.float32), "iter": t + 1, "time": total_time}
    
    return out
