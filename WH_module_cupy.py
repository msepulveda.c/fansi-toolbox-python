

import time
import numpy as np
try:
    import cupy as cp
    num_gpus = cp.cuda.runtime.getDeviceCount()
    if num_gpus > 0:
        gpu_available = True
    else:
        gpu_available = False
except (ImportError, cp.cuda.runtime.CUDARuntimeError):
    cp = None
    gpu_available = False

def WH_TV(params):
    """
    
    Nonlinear/Linear Weak Harmonics - QSM and Total Variation regularization
    with spatially variable fidelity and regularization weights.

    This uses ADMM to solve the functional. 
    
    This function is used to remove background field remnants from *local*
    field maps and calculate the susceptibility of tissues simultaneously.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map
    params.K: dipole kernel in the frequency space
    params.alpha1: gradient penalty (L1-norm) or regularization weight
    Optional fields:
    params.beta: harmonic constrain weight (default value = 150)
    params.muH: harmonic consistency weight (recommended value = beta/50)
    params.mask: ROI to calculate susceptibility values (if not provided, will be calculated from 'weight')
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1)
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0)
    params.maxOuterIter: maximum number of iterations (recommended for testing = 150, for correct convergence of the harmonic field hundreds of iterations are needed)
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1)
    params.tolDelta: (recommended = 1e-6)
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data). 
    params.regWeight: regularization spatially variable weight.
    params.isGPU: GPU acceleration (default = true)
    params.isNonlinear: Linear or nonlinear algorithm?(default = true)
    params.voxelSize: voxel size (default value = [1,1,1])

    Output: out - structure with the following fields:
    out.x: calculated susceptibility map
    out.iter: number of iterations needed
    out.time: total elapsed time (including pre-calculations)
    
    """
    
    start_time = time.time()

    isGPU = params.get("isGPU", True)
    
    if isGPU and gpu_available:
        xp = cp
        print("GPU")
    else:
        xp = np
        print("CPU")
        
    # Required parameters
    Kernel = xp.array(params["K"], dtype=xp.float32)
    phase = xp.array(params["input"], dtype=xp.float64)

    alpha1 = xp.array(params.get("alpha", 1E-5), dtype=xp.float32)
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)

    weight = xp.array(params.get("weight", xp.ones(N)), dtype=xp.float32)
    W = weight * weight
    mu1 = params.get("mu1", 100 * alpha1).astype(xp.float32)
    mu2 = xp.array(params.get("mu2", 1), dtype=xp.float32)
    muH = xp.array(params.get("muH", 5), dtype=xp.float32)
    beta = xp.array(params.get("beta", 150), dtype=xp.float32)
    mask = params.get("mask", (W > 0)).astype(xp.int32)
    #mask = W > 0
    
    tolUpdate = xp.array(params.get("tolUpdate", 0.1))
    voxelSize = xp.array(params.get("voxelSize", [[1, 1, 1]]))
    regWeight = xp.ones(N + (3,), dtype=xp.float32)
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = xp.array(params.get("tolDelta", 1E-6))
        z2 = phase * W
    else:
        # Redefinition of variable for computational efficiency
        Wy = (W * phase) / (W + mu2)
        z2 = Wy
    
    # Variable initialization
    z_dx = xp.zeros(N, dtype=xp.float32)
    z_dy = xp.zeros(N, dtype=xp.float32)
    z_dz = xp.zeros(N, dtype=xp.float32)

    s_dx = xp.zeros(N, dtype=xp.float32)
    s_dy = xp.zeros(N, dtype=xp.float32)
    s_dz = xp.zeros(N, dtype=xp.float32)

    x = xp.zeros(N, dtype=xp.float32)
    
    phi_h = xp.zeros(N, dtype=xp.float32)
    
    z_h = xp.zeros(N, dtype=xp.float32)
    s_h = xp.zeros(N, dtype=xp.float32)

    s2 = xp.zeros(N, dtype=xp.float32)

    alpha_over_mu = (alpha1 / mu1).astype(xp.float32)  # for efficiency
    
    # Define the operators
    k1, k2, k3 = xp.meshgrid(xp.arange(N[0]), xp.arange(N[1]), xp.arange(N[2]), indexing='xy')

    E1 = (1 - xp.exp(2j * xp.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - xp.exp(2j * xp.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - xp.exp(2j * xp.pi * k3 / N[2])) / voxelSize[0, 2]
    
    
    E1t = xp.conj(E1)
    E2t = xp.conj(E2)
    E3t = xp.conj(E3)
    Kt = xp.conj(Kernel)
    
    EE2 = E1t * E1 + E2t * E2 + E3t * E3    
    
    regTime = time.time()
    for t in range(maxOuterIter):
        # Update x: susceptibility estimate
        tx = E1t * xp.fft.fftn(z_dx - s_dx)
        ty = E2t * xp.fft.fftn(z_dy - s_dy)
        tz = E3t * xp.fft.fftn(z_dz - s_dz)

        x_prev = x.clone()
        Dt_kspace = Kt * xp.fft.fftn(z2 - s2 -phi_h);
        x = mask * xp.real(xp.fft.ifftn((mu1 * (tx + ty + tz) + Dt_kspace) / (xp.finfo(xp.float64).eps + mu2 * xp.abs(Kernel) ** 2 + mu1 * EE2)));
    
        del tx
        del ty
        del tz 
            
        x_update = 100 * xp.linalg.norm(x - x_prev) / xp.linalg.norm(x)
        
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
            
        if x_update < tolUpdate or xp.isnan(x_update):
            print("\nEarly stopping reached.")
            break

        if t < maxOuterIter:
            # Update z: gradient variable
            Fx = xp.fft.fftn(x)
            x_dx = xp.real(xp.fft.ifftn(E1 * Fx))
            x_dy = xp.real(xp.fft.ifftn(E2 * Fx))
            x_dz = xp.real(xp.fft.ifftn(E3 * Fx))

            z_dx = xp.maximum(xp.abs(x_dx + s_dx) - regWeight[:, :, :, 0] * alpha_over_mu, 0) * xp.sign(x_dx + s_dx)
            z_dy = xp.maximum(xp.abs(x_dy + s_dy) - regWeight[:, :, :, 1] * alpha_over_mu, 0) * xp.sign(x_dy + s_dy)
            z_dz = xp.maximum(xp.abs(x_dz + s_dz) - regWeight[:, :, :, 2] * alpha_over_mu, 0) * xp.sign(x_dz + s_dz)

            # Update s: Lagrange multiplier
            s_dx = s_dx + x_dx - z_dx
            s_dy = s_dy + x_dy - z_dy
            s_dz = s_dz + x_dz - z_dz

            del x_dx
            del x_dy
            del x_dz                

            # Update z2 and s2: data consistency
            if isNonlinear:
                rhs_z2 = mu2 * xp.real(xp.fft.ifftn(Kernel * Fx) + s2 +phi_h)
                z2 =  rhs_z2 / mu2
            
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                while (delta > tolDelta and inn < 50):
                    inn = inn + 1;
                    norm_old = xp.linalg.norm(z2.reshape(-1))
                    
                    update = (W * xp.sin(z2 - phase) + mu2 * z2 - rhs_z2) / (W * xp.cos(z2 - phase) + mu2)            
                
                    z2 = z2 - update
                    delta = xp.linalg.norm(update.reshape(-1)) / norm_old             
                    
                del rhs_z2
                del update
                
                Fphi_h = (muH * xp.conj(EE2) * xp.fft.fftn(z_h - s_h) + mu2 * xp.fft.fftn(z2 - s2) - mu2 * Kernel * Fx) / (xp.finfo(xp.float64).eps + mu2 + muH * EE2 * xp.conj(EE2))
                phi_h = xp.real(xp.fft.ifftn(Fphi_h))
                
            else:
                z2 = Wy + mu2 * xp.real(xp.fft.ifftn(Kernel * Fx) + s2 + phi_h) / (W + mu2)
                           
                phi_h = xp.real(xp.fft.ifftn((muH * xp.conj(EE2) * xp.fft.fftn(z_h - s_h) + mu2 * xp.fft.fftn(z2-s2) - mu2 * Kernel * Fx) / (xp.finfo(xp.float64).eps + mu2 + muH * EE2 * xp.conj(EE2))))
                Fphi_h = xp.fft.fftn(phi_h)
            
            z_h = muH * (xp.real(xp.fft.ifftn(EE2 * Fphi_h)) + s_h) / (muH + beta * mask)
            s2 = s2 + xp.real(xp.fft.ifftn(Kernel * Fx)) - z2 + phi_h
              
            s_h = s_h + xp.real(xp.fft.ifftn(EE2 * Fphi_h)) - z_h
                
            del Fx
            del Fphi_h    
          
    print('\n')
    
    # Extract output values
    end_time = time.time()
    reg_time = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')

    if isGPU and gpu_available:
        out = {"x": cp.asnumpy(x).astype(np.float32), "iter": t + 1, "time": total_time}
    else:
        out = {"x": x.astype(np.float32), "iter": t + 1, "time": total_time}
    
    return out


def WH_TGV(params):
    """

    Nonlinear/Linear QSM and Total Generalized Variation regularization with
    spatially variable fidelity and regularization weights.

    This uses ADMM to solve the functional.
    
    This function is used to remove background field remnants from *local*
    field maps and calculate the susceptibility of tissues simultaneously.

    Parameters: params - structure with 
    Required fields:
    params.input: local field map
    params.K: dipole kernel in the frequency space
    params.alpha1: gradient penalty (L1-norm) or regularization weight
    Optional fields:
    params.beta: harmonic constrain weight (default value = 150)
    params.muH: harmonic consistency weight (recommended value = beta/50)
    params.mask: ROI to calculate susceptibility values (if not provided, will be calculated from 'weight')
    params.alpha0: curvature L1 penalty, regularization weight (recommended alpha0 = 2*alpha1)
    params.mu0: curvature consistency weight (ADMM weight, recommended = 100*alpha0)
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1)
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0)
    params.maxOuterIter: maximum number of iterations (recommended for testing = 150, for correct convergence of the harmonic field hundreds of iterations are needed)
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1)
    params.tolDelta: (recommended = 1e-6)
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data). 
    params.regWeight: regularization spatially variable weight.
    params.isGPU: GPU acceleration (default = true)
    params.isNonlinear: Linear or nonlinear algorithm?(default = true)
    params.voxelSize: voxel size (default value = [1,1,1])

    Output: out - structure with the following fields:
    out.x: calculated susceptibility map
    out.iter: number of iterations needed
    out.time: total elapsed time (including pre-calculations)

    """
    
    start_time = time.time()

    isGPU = params.get("isGPU", True)
    
    if isGPU and gpu_available:
        xp = cp
        print("GPU")
    else:
        xp = np
        print("CPU")
        
    # Required parameters
    Kernel = xp.array(params["K"], dtype=xp.float32)
    phase = xp.array(params["input"], dtype=xp.float64)
    
    alpha1 = xp.array(params.get("alpha", 1E-5), dtype=xp.float32)
    alpha0 = params.get("alpha0", 2 * alpha1).astype(xp.float32)
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)
    
    weight = xp.array(params.get("weight", xp.ones(N)), dtype=xp.float32)
    W = weight * weight
    mu0 = params.get("mu0", 200 * alpha1).astype(xp.float32)
    mu1 = params.get("mu1", 100 * alpha1).astype(xp.float32)
    mu2 = xp.array(params.get("mu2", 1), dtype=xp.float32)
    muH = xp.array(params.get("muH", 5), dtype=xp.float32)
    beta = xp.array(params.get("beta", 150), dtype=xp.float32)
    mask = params.get("mask", (W > 0)).astype(xp.int32)
    
    tolUpdate = xp.array(params.get("tolUpdate", 0.1))
    voxelSize = xp.array(params.get("voxelSize", [[1, 1, 1]]))
    regWeight = xp.ones(N + (3,), dtype=xp.float32)
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = xp.array(params.get("tolDelta", 1E-6))
        z2 =  xp.fft.fftn(W * phase / (W + mu2))
    else:
        # Redefine variable for computational efficiency
        FWy = xp.fft.fftn(W * phase / (W + mu2))
        z2 = FWy  # start with something similar to the input phase, weighted to reduce noise
              
    alpha_over_mu = (alpha1 / mu1).astype(xp.float32)  # for efficiency
    alpha0_over_mu0 = (alpha0 / mu0).astype(xp.float32)  # for efficiency
    
    # Precompute gradient-related matrices
    k1, k2, k3 = xp.meshgrid(xp.arange(N[0]), xp.arange(N[1]), xp.arange(N[2]), indexing='xy')
    E1 = (1 - xp.exp(2j * xp.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - xp.exp(2j * xp.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - xp.exp(2j * xp.pi * k3 / N[2])) / voxelSize[0, 2]

    phi_h = xp.zeros(N, dtype=xp.float32)
    
    z_h = xp.zeros(N, dtype=xp.float32)
    s_h = xp.zeros(N, dtype=xp.float32)

    s2 = xp.zeros(N, dtype=xp.float32)
   
    # Allocate memory for first order gradient
    s1_1 = xp.zeros(N, dtype=xp.float32)
    s1_2 = xp.zeros(N, dtype=xp.float32)
    s1_3 = xp.zeros(N, dtype=xp.float32)
    z1_1 = xp.zeros(N, dtype=xp.float32)
    z1_2 = xp.zeros(N, dtype=xp.float32)
    z1_3 = xp.zeros(N, dtype=xp.float32)

    # Allocate memory for symmetrized gradient
    s0_1 = xp.zeros(N, dtype=xp.float32)
    s0_2 = xp.zeros(N, dtype=xp.float32)
    s0_3 = xp.zeros(N, dtype=xp.float32)
    s0_4 = xp.zeros(N, dtype=xp.float32)
    s0_5 = xp.zeros(N, dtype=xp.float32)
    s0_6 = xp.zeros(N, dtype=xp.float32)
    z0_1 = xp.zeros(N, dtype=xp.float32)
    z0_2 = xp.zeros(N, dtype=xp.float32)
    z0_3 = xp.zeros(N, dtype=xp.float32)
    z0_4 = xp.zeros(N, dtype=xp.float32)
    z0_5 = xp.zeros(N, dtype=xp.float32)
    z0_6 = xp.zeros(N, dtype=xp.float32)

    x = xp.zeros(N, dtype=xp.float32)
    
    Et1 = xp.conj(E1)
    Et2 = xp.conj(E2)
    Et3 = xp.conj(E3)
    Kt = xp.conj(Kernel)
    
    E1tE1 = Et1 * E1
    E2tE2 = Et2 * E2
    E3tE3 = Et3 * E3
    EE2 = E1tE1 + E2tE2 + E3tE3
    mu0_over_2_E1tE2 = mu0 / 2 * Et1 * E2
    mu0_over_2_E1tE3 = mu0 / 2 * Et1 * E3
    mu0_over_2_E2tE3 = mu0 / 2 * Et2 * E3
    
    KtK_mu1_E_sos = mu2 * Kt * Kernel + mu1 * (E1tE1 + E2tE2 + E3tE3)
    mu1I_mu0_E_wsos1 = mu1 + mu0 * (E1tE1 + (E2tE2 + E3tE3) / 2)
    mu1I_mu0_E_wsos2 = mu1 + mu0 * (E1tE1 / 2 + E2tE2 + E3tE3 / 2)
    mu1I_mu0_E_wsos3 = mu1 + mu0 * ((E1tE1 + E2tE2) / 2 + E3tE3)

    del E1tE1  
    del E2tE2
    del E3tE3
    
    # Precomputation for Cramer's Rule
    a1 = KtK_mu1_E_sos
    a2 = mu1I_mu0_E_wsos1
    a3 = mu1I_mu0_E_wsos2
    a4 = mu1I_mu0_E_wsos3
    a5 = -mu1 * E1
    a6 = -mu1 * E2
    a7 = mu0_over_2_E1tE2
    a8 = -mu1 * E3
    a9 = mu0_over_2_E1tE3
    a10 = mu0_over_2_E2tE3
    
    a5t = xp.conj(a5)
    a6t = xp.conj(a6)
    a7t = xp.conj(a7)
    a8t = xp.conj(a8)
    a9t = xp.conj(a9)
    a10t = xp.conj(a10)
    
    # Free GPU memory for specific variables
    del KtK_mu1_E_sos
    del mu1I_mu0_E_wsos1
    del mu1I_mu0_E_wsos2
    del mu1I_mu0_E_wsos3
    
    # For x
    D11 = a2 * a3 * a4 + a7t * a9 * a10t + a7 * a9t * a10 - a3 * a9 * a9t - a2 * a10 * a10t - a4 * a7 * a7t
    D21 = a3 * a4 * a5t + a6t * a9 * a10t + a7 * a8t * a10 - a3 * a8t * a9 - a5t * a10 * a10t - a4 * a6t * a7
    D31 = a4 * a5t * a7t + a6t * a9 * a9t + a2 * a8t * a10 - a7t * a8t * a9 - a5t * a9t * a10 - a2 * a4 * a6t
    D41 = a5t * a7t * a10t + a6t * a7 * a9t + a2 * a3 * a8t - a7 * a7t * a8t - a3 * a5t * a9t - a2 * a6t * a10t

    # For vx
    D12 = a3 * a4 * a5 + a7t * a8 * a10t + a6 * a9t * a10 - a3 * a8 * a9t - a5 * a10 * a10t - a4 * a6 * a7t
    D22 = a1 * a3 * a4 + a6t * a8 * a10t + a6 * a8t * a10 - a3 * a8 * a8t - a1 * a10 * a10t - a4 * a6 * a6t
    D32 = a1 * a4 * a7t + a6t * a8 * a9t + a5 * a8t * a10 - a7t * a8 * a8t - a1 * a9t * a10 - a4 * a5 * a6t
    D42 = a1 * a7t * a10t + a6 * a6t * a9t + a3 * a5 * a8t - a6 * a7t * a8t - a1 * a3 * a9t - a5 * a6t * a10t

    # For vy
    D13 = a4 * a5 * a7 + a2 * a8 * a10t + a6 * a9 * a9t - a7 * a8 * a9t - a5 * a9 * a10t - a2 * a4 * a6
    D23 = a1 * a4 * a7 + a5t * a8 * a10t + a6 * a8t * a9 - a7 * a8 * a8t - a1 * a9 * a10t - a4 * a5t * a6
    D33 = a1 * a2 * a4 + a5t * a8 * a9t + a5 * a8t * a9 - a2 * a8 * a8t - a1 * a9 * a9t - a4 * a5 * a5t
    D43 = a1 * a2 * a10t + a5t * a6 * a9t + a5 * a7 * a8t - a2 * a6 * a8t - a1 * a7 * a9t - a5 * a5t * a10t

    # For vz
    D14 = a5 * a7 * a10 + a2 * a3 * a8 + a6 * a7t * a9 - a7 * a7t * a8 - a3 * a5 * a9 - a2 * a6 * a10
    D24 = a1 * a7 * a10 + a3 * a5t * a8 + a6 * a6t * a9 - a6t * a7 * a8 - a1 * a3 * a9 - a5t * a6 * a10
    D34 = a1 * a2 * a10 + a5t * a7t * a8 + a5 * a6t * a9 - a2 * a6t * a8 - a1 * a7t * a9 - a5 * a5t * a10
    D44 = a1 * a2 * a3 + a5t * a6 * a7t + a5 * a6t * a7 - a2 * a6 * a6t - a1 * a7 * a7t - a3 * a5 * a5t

    det_A = a1 * D11 - a5 * D21 + a6 * D31 - a8 * D41
    det_Ainv = 1 / (xp.finfo(xp.float64).eps + det_A)
    
    # Free GPU memory for specific variables
    del mu0_over_2_E1tE2
    del mu0_over_2_E1tE3
    del mu0_over_2_E2tE3
    del a1
    del a2
    del a3
    del a4
    del a5
    del a6
    del a7
    del a8
    del a9
    del a10
    del a6t
    del a7t
    del a8t
    del a9t
    del a10t
    del det_A
    
    # You can clear additional variables as needed
    # del D11, D21, D31, D41, D12, D22, D32, D42, D13, D23, D33, D43, D14, D24, D34, D44
    
    reg_time = time.time()
    
    for t in range(maxOuterIter):
        x_prev = x
        # Update x and v: susceptibility estimate
        F_z0_minus_s0_1 = xp.fft.fftn(z0_1 - s0_1)
        F_z0_minus_s0_2 = xp.fft.fftn(z0_2 - s0_2)
        F_z0_minus_s0_3 = xp.fft.fftn(z0_3 - s0_3)
        F_z0_minus_s0_4 = xp.fft.fftn(z0_4 - s0_4)
        F_z0_minus_s0_5 = xp.fft.fftn(z0_5 - s0_5)
        F_z0_minus_s0_6 = xp.fft.fftn(z0_6 - s0_6)
    
        F_z1_minus_s1_1 = xp.fft.fftn(z1_1 - s1_1)
        F_z1_minus_s1_2 = xp.fft.fftn(z1_2 - s1_2)
        F_z1_minus_s1_3 = xp.fft.fftn(z1_3 - s1_3)
        
        mu2_Kt = mu2 * Kt
        Et1_F_z1_minus_s1_1 = Et1 * F_z1_minus_s1_1
        Et2_F_z1_minus_s1_2 = Et2 * F_z1_minus_s1_2
        Et3_F_z1_minus_s1_3 = Et3 * F_z1_minus_s1_3
        
        rhs1 = mu2_Kt * (z2 - s2 - xp.fft.fftn(phi_h)) + mu1 * (Et1_F_z1_minus_s1_1 + Et2_F_z1_minus_s1_2 + Et3_F_z1_minus_s1_3)
        rhs2 = -mu1 * F_z1_minus_s1_1 + mu0 * (Et1 * F_z0_minus_s0_1 + Et2 * F_z0_minus_s0_4 + Et3 * F_z0_minus_s0_5)
        rhs3 = -mu1 * F_z1_minus_s1_2 + mu0 * (Et2 * F_z0_minus_s0_2 + Et1 * F_z0_minus_s0_4 + Et3 * F_z0_minus_s0_6)
        rhs4 = -mu1 * F_z1_minus_s1_3 + mu0 * (Et3 * F_z0_minus_s0_3 + Et1 * F_z0_minus_s0_5 + Et2 * F_z0_minus_s0_6)
            
        # Free GPU memory for specific variables
        del F_z0_minus_s0_1
        del F_z0_minus_s0_2
        del F_z0_minus_s0_3
        del F_z0_minus_s0_4
        del F_z0_minus_s0_5
        del F_z0_minus_s0_6
        del F_z1_minus_s1_1
        del F_z1_minus_s1_2
        del F_z1_minus_s1_3

        # Cramer's rul
        Fx = (rhs1 * D11 - rhs2 * D21 + rhs3 * D31 - rhs4 * D41) * det_Ainv
        Fv1 = (-rhs1 * D12 + rhs2 * D22 - rhs3 * D32 + rhs4 * D42) * det_Ainv
        Fv2 = (rhs1 * D13 - rhs2 * D23 + rhs3 * D33 - rhs4 * D43) * det_Ainv
        Fv3 = (-rhs1 * D14 + rhs2 * D24 - rhs3 * D34 + rhs4 * D44) * det_Ainv
            
        # Free GPU memory for specific variables
        del rhs1
        del rhs2
        del rhs3
        del rhs4
            
        x = mask * xp.real(xp.fft.ifftn(Fx))
        Fx = xp.fft.fftn(x)  # Extra step to increase stability from the proximal step
        v1 = xp.real(xp.fft.ifftn(Fv1))
        v2 = xp.real(xp.fft.ifftn(Fv2))
        v3 = xp.real(xp.fft.ifftn(Fv3))
    
        x_update = 100 * xp.linalg.norm(x - x_prev) / xp.linalg.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
          
        if x_update < tolUpdate:
            print("\nEarly stopping reached.")
            break
        
        if t < maxOuterIter:
        
            # Compute gradients for z0 and z1 update
            Dx1 = xp.real(xp.fft.ifftn(E1 * Fx))
            Dx2 = xp.real(xp.fft.ifftn(E2 * Fx))
            Dx3 = xp.real(xp.fft.ifftn(E3 * Fx))
        
            E_v1 = xp.real(xp.fft.ifftn(E1 * Fv1))
            E_v2 = xp.real(xp.fft.ifftn(E2 * Fv2))
            E_v3 = xp.real(xp.fft.ifftn(E3 * Fv3))
            E_v4 = xp.real(xp.fft.ifftn(E1 * Fv2 + E2 * Fv1)) / 2
            E_v5 = xp.real(xp.fft.ifftn(E1 * Fv3 + E3 * Fv1)) / 2
            E_v6 = xp.real(xp.fft.ifftn(E2 * Fv3 + E3 * Fv2)) / 2
            
            # Free GPU memory for specific variables
            del Fv1
            del Fv2
            del Fv3
            
            # Update z0: Symm grad
            z0_1 = xp.maximum(xp.abs(E_v1 + s0_1) - alpha0_over_mu0, 0) * xp.sign(E_v1 + s0_1)
            z0_2 = xp.maximum(xp.abs(E_v2 + s0_2) - alpha0_over_mu0, 0) * xp.sign(E_v2 + s0_2)
            z0_3 = xp.maximum(xp.abs(E_v3 + s0_3) - alpha0_over_mu0, 0) * xp.sign(E_v3 + s0_3)
            z0_4 = xp.maximum(xp.abs(E_v4 + s0_4) - alpha0_over_mu0, 0) * xp.sign(E_v4 + s0_4)
            z0_5 = xp.maximum(xp.abs(E_v5 + s0_5) - alpha0_over_mu0, 0) * xp.sign(E_v5 + s0_5)
            z0_6 = xp.maximum(xp.abs(E_v6 + s0_6) - alpha0_over_mu0, 0) * xp.sign(E_v6 + s0_6)
                 
            # Update z1: Grad
            z1_1 = xp.maximum(xp.abs(Dx1 - v1 + s1_1) - regWeight[..., 0] * alpha_over_mu, 0) * xp.sign(Dx1 - v1 + s1_1)
            z1_2 = xp.maximum(xp.abs(Dx2 - v2 + s1_2) - regWeight[..., 1] * alpha_over_mu, 0) * xp.sign(Dx2 - v2 + s1_2)
            z1_3 = xp.maximum(xp.abs(Dx3 - v3 + s1_3) - regWeight[..., 2] * alpha_over_mu, 0) * xp.sign(Dx3 - v3 + s1_3)
            
            # Update z2 and s2: data consistency
            rhs_z2 = mu2 * xp.real(xp.fft.ifftn(Kernel * Fx + s2 + phi_h))
            
            if isNonlinear:
                z2 = rhs_z2 / mu2
                    
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                while (delta > tolDelta and inn < 50):
                    inn = inn + 1
                    norm_old = xp.linalg.norm(z2)
                
                    update = (W * xp.sin(z2 - phase) + mu2 * z2 - rhs_z2) / (W * xp.cos(z2 - phase) + mu2)
            
                    z2 -= update
                    delta = xp.linalg.norm(update) / norm_old
                z2 = xp.fft.fftn(z2)    
                # Free GPU memory for specific variables
                del update
            else:
                z2 = FWy + xp.fft.fftn(rhs_z2 / (W + mu2))
                
            Fphi_h = (muH * xp.conj(EE2) * xp.fft.fftn(z_h - s_h) + mu2 * (z2 - s2) - mu2 * Kernel * Fx) / (xp.finfo(xp.float64).eps + mu2 + muH * EE2 * xp.conj(EE2))
            phi_h = xp.real(xp.fft.ifftn(Fphi_h))
            z_h = muH * (xp.real(xp.fft.ifftn(EE2 * Fphi_h)) + s_h) / (muH + beta * mask)
            s2 = s2 + Kernel * Fx - z2 + xp.fft.fftn(phi_h)
                
            # Update s0 and s1
            s0_1 = s0_1 + E_v1 - z0_1
            s0_2 = s0_2 + E_v2 - z0_2
            s0_3 = s0_3 + E_v3 - z0_3
            s0_4 = s0_4 + E_v4 - z0_4
            s0_5 = s0_5 + E_v5 - z0_5
            s0_6 = s0_6 + E_v6 - z0_6
        
            s1_1 = s1_1 + Dx1 - v1 - z1_1
            s1_2 = s1_2 + Dx2 - v2 - z1_2
            s1_3 = s1_3 + Dx3 - v3 - z1_3
            
            # Free GPU memory for specific variables
            del rhs_z2
            del Dx1
            del Dx2
            del Dx3
            del E_v1
            del E_v2
            del E_v3
            del E_v4
            del E_v5
            del E_v6
            
            s_h = s_h + xp.real(xp.fft.ifftn(EE2 * Fphi_h)) - z_h
            
    # Extract output values
    end_time = time.time()
    reg_time = end_time - reg_time
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')
    
    if isGPU and gpu_available:
        out = {"x": cp.asnumpy(x).astype(np.float32), "iter": t + 1, "time": total_time}
    else:
        out = {"x": x.astype(np.float32), "iter": t + 1, "time": total_time}
    
    return out