

import time
import numpy as np
import torch
import torch.fft as fft


def WH_TV(params):
    """
    
    Nonlinear/Linear Weak Harmonics - QSM and Total Variation regularization
    with spatially variable fidelity and regularization weights.

    This uses ADMM to solve the functional. 
    
    This function is used to remove background field remnants from *local*
    field maps and calculate the susceptibility of tissues simultaneously.
    
    Parameters: params - structure with 
    Required fields:
    params.input: local field map
    params.K: dipole kernel in the frequency space
    params.alpha1: gradient penalty (L1-norm) or regularization weight
    Optional fields:
    params.beta: harmonic constrain weight (default value = 150)
    params.muH: harmonic consistency weight (recommended value = beta/50)
    params.mask: ROI to calculate susceptibility values (if not provided, will be calculated from 'weight')
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1)
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0)
    params.maxOuterIter: maximum number of iterations (recommended for testing = 150, for correct convergence of the harmonic field hundreds of iterations are needed)
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1)
    params.tolDelta: (recommended = 1e-6)
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data). 
    params.regWeight: regularization spatially variable weight.
    params.isNonlinear: Linear or nonlinear algorithm?(default = true)
    params.voxelSize: voxel size (default value = [1,1,1])

    Output: out - structure with the following fields:
    out.x: calculated susceptibility map
    out.iter: number of iterations needed
    out.time: total elapsed time (including pre-calculations)
    
    """
    
    start_time = time.time()
    # Required parameters
    Kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha1 = torch.tensor(params.get("alpha", 1E-5), dtype=torch.float)
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", np.ones(N)), dtype=torch.float32)
    W = weight * weight    
    mu1 = params.get("mu1", 100 * alpha1).clone().detach().requires_grad_(False).to(torch.float32)
    mu2 = torch.tensor(params.get("mu2", 1), dtype=torch.float32)
    muH = torch.tensor(params.get("muH", 5), dtype=torch.float32)
    beta = torch.tensor(params.get("beta", 150), dtype=torch.float32)
    mask_a = (W > 0).to(torch.int64)
    mask = params.get("mask", mask_a).clone().detach().requires_grad_(False).to(torch.int32)
    #mask = W > 0
    
    tolUpdate = torch.tensor(params.get("tolUpdate", 0.1))
    voxelSize = np.array(params.get("voxelSize", [[1,1,1]]))
    regWeight = torch.from_numpy(np.ones(N + (3,)))
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = torch.tensor(params.get("tolDelta", 1E-6))
        z2 = phase * W
    else:
        # Redefinition of variable for computational efficiency
        Wy = (W * phase) / (W + mu2)
        z2 = Wy
    
    # Variable initialization
    z_dx = torch.zeros(N, dtype=torch.float32)
    z_dy = torch.zeros(N, dtype=torch.float32)
    z_dz = torch.zeros(N, dtype=torch.float32)

    s_dx = torch.zeros(N, dtype=torch.float32)
    s_dy = torch.zeros(N, dtype=torch.float32)
    s_dz = torch.zeros(N, dtype=torch.float32)

    x = torch.zeros(N, dtype=torch.float32)
    
    phi_h = torch.zeros(N, dtype=torch.float32)
    
    z_h = torch.zeros(N, dtype=torch.float32)
    s_h = torch.zeros(N, dtype=torch.float32)

    s2 = torch.zeros(N, dtype=torch.float32)

    alpha_over_mu = (alpha1 / mu1).clone().detach().requires_grad_(False).to(torch.float32) # for efficiency

    # Define the operators
    k1, k2, k3 = torch.meshgrid(torch.arange(N[0]), torch.arange(N[1]), torch.arange(N[2]), indexing='xy')

    E1 = (1 - torch.exp(2j * torch.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - torch.exp(2j * torch.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - torch.exp(2j * torch.pi * k3 / N[2])) / voxelSize[0, 2]

    zero = torch.tensor(0) 
    
    torch.cuda.empty_cache()
    # Move variables to GPU if available  
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if isNonlinear:
        phase = phase.to(device)
    else:
        Wy = Wy.to(device)
    z_dx = z_dx.to(device)
    z_dy = z_dy.to(device)
    z_dz = z_dz.to(device)
    
    s_dx = s_dx.to(device)
    s_dy = s_dy.to(device)
    s_dz = s_dz.to(device)
    
    x = x.to(device)
    Kernel = Kernel.to(device)
    
    z2 = z2.to(device)
    s2 = s2.to(device)
    
    tolUpdate = tolUpdate.to(device)
    
    E1 = E1.to(device)
    E2 = E2.to(device)
    E3 = E3.to(device)
    
    alpha_over_mu = alpha_over_mu.to(device)
    W = W.to(device)
    mu1 = mu1.to(device)
    mu2 = mu2.to(device)
    regWeight = regWeight.to(device)
    zero = zero.to(device)
    
    phi_h = phi_h.to(device)
    z_h = z_h.to(device)
    s_h = s_h.to(device)
    muH = muH.to(device)
    beta = beta.to(device)
    mask = mask.to(device)
    
    E1t = torch.conj(E1)
    E2t = torch.conj(E2)
    E3t = torch.conj(E3)

    EE2 = E1t * E1 + E2t * E2 + E3t * E3
    
    Kt = torch.conj(Kernel)
    regTime = time.time()
    
    for t in range(maxOuterIter):
        # Update x: susceptibility estimate
        tx = E1t * fft.fftn(z_dx - s_dx)
        ty = E2t * fft.fftn(z_dy - s_dy)
        tz = E3t * fft.fftn(z_dz - s_dz)

        x_prev = x.clone()
        Dt_kspace = Kt * fft.fftn(z2 - s2 -phi_h);
        x = mask * torch.real(fft.ifftn((mu1 * (tx + ty + tz) + Dt_kspace) / (torch.finfo(torch.float64).eps + mu2 * torch.abs(Kernel) ** 2 + mu1 * EE2)));
    
        del tx
        del ty
        del tz 
            
        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
            
        if x_update < tolUpdate or torch.isnan(x_update):
            print("\nEarly stopping reached.")
            break

        if t < maxOuterIter:
            # Update z: gradient variable
            Fx = fft.fftn(x)
            x_dx = torch.real(fft.ifftn(E1 * Fx))
            x_dy = torch.real(fft.ifftn(E2 * Fx))
            x_dz = torch.real(fft.ifftn(E3 * Fx))

            z_dx = torch.maximum(torch.abs(x_dx + s_dx) - regWeight[:, :, :, 0] * alpha_over_mu, zero) * torch.sign(x_dx + s_dx)
            z_dy = torch.maximum(torch.abs(x_dy + s_dy) - regWeight[:, :, :, 1] * alpha_over_mu, zero) * torch.sign(x_dy + s_dy)
            z_dz = torch.maximum(torch.abs(x_dz + s_dz) - regWeight[:, :, :, 2] * alpha_over_mu, zero) * torch.sign(x_dz + s_dz)

            # Update s: Lagrange multiplier
            s_dx = s_dx + x_dx - z_dx
            s_dy = s_dy + x_dy - z_dy
            s_dz = s_dz + x_dz - z_dz

            del x_dx
            del x_dy
            del x_dz                

            # Update z2 and s2: data consistency
            if isNonlinear:
                rhs_z2 = mu2 * torch.real(fft.ifftn(Kernel * Fx) + s2 +phi_h)
                z2 =  rhs_z2 / mu2
            
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                while (delta > tolDelta and inn < 50):
                    inn = inn + 1;
                    norm_old = torch.norm(z2.reshape(-1))
                    
                    update = (W * torch.sin(z2 - phase) + mu2 * z2 - rhs_z2) / (W * torch.cos(z2 - phase) + mu2)            
                
                    z2 = z2 - update
                    delta = torch.norm(update.reshape(-1)) / norm_old             
                    
                del rhs_z2
                del update
                
                Fphi_h = (muH * torch.conj(EE2) * fft.fftn(z_h - s_h) + mu2 * fft.fftn(z2 - s2) - mu2 * Kernel * Fx) / (torch.finfo(torch.float64).eps + mu2 + muH * EE2 * torch.conj(EE2))
                phi_h = torch.real(fft.ifftn(Fphi_h))
                
            else:
                z2 = Wy + mu2 * torch.real(fft.ifftn(Kernel * Fx) + s2 + phi_h) / (W + mu2)
                           
                phi_h = torch.real(fft.ifftn((muH * torch.conj(EE2) * fft.fftn(z_h - s_h) + mu2 * fft.fftn(z2-s2) - mu2 * Kernel * Fx) / (torch.finfo(torch.float64).eps + mu2 + muH * EE2 * torch.conj(EE2))))
                Fphi_h = fft.fftn(phi_h)
            
            z_h = muH * (torch.real(fft.ifftn(EE2 * Fphi_h)) + s_h) / (muH + beta * mask)
            s2 = s2 + torch.real(fft.ifftn(Kernel * Fx)) - z2 + phi_h
              
            s_h = s_h + torch.real(fft.ifftn(EE2 * Fphi_h)) - z_h
                
            del Fx
            del Fphi_h    
          
    print('\n')
    
    # Extract output values
    end_time = time.time()
    reg_time = end_time - regTime
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')

    out = {"x": x.cpu().detach().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    
    return out


def WH_TGV(params):
    """

    Nonlinear/Linear QSM and Total Generalized Variation regularization with
    spatially variable fidelity and regularization weights.

    This uses ADMM to solve the functional.
    
    This function is used to remove background field remnants from *local*
    field maps and calculate the susceptibility of tissues simultaneously.

    Parameters: params - structure with 
    Required fields:
    params.input: local field map
    params.K: dipole kernel in the frequency space
    params.alpha1: gradient penalty (L1-norm) or regularization weight
    Optional fields:
    params.beta: harmonic constrain weight (default value = 150)
    params.muH: harmonic consistency weight (recommended value = beta/50)
    params.mask: ROI to calculate susceptibility values (if not provided, will be calculated from 'weight')
    params.alpha0: curvature L1 penalty, regularization weight (recommended alpha0 = 2*alpha1)
    params.mu0: curvature consistency weight (ADMM weight, recommended = 100*alpha0)
    params.mu1: gradient consistency weight (ADMM weight, recommended = 100*alpha1)
    params.mu2: fidelity consistency weight (ADMM weight, recommended value = 1.0)
    params.maxOuterIter: maximum number of iterations (recommended for testing = 150, for correct convergence of the harmonic field hundreds of iterations are needed)
    params.tolUpdate: convergence limit, update ratio of the solution (recommended = 0.1)
    params.tolDelta: (recommended = 1e-6)
    params.weight: data fidelity spatially variable weight (recommended = magnitude_data). 
    params.regWeight: regularization spatially variable weight.
    params.isNonlinear: Linear or nonlinear algorithm?(default = true)
    params.voxelSize: voxel size (default value = [1,1,1])

    Output: out - structure with the following fields:
    out.x: calculated susceptibility map
    out.iter: number of iterations needed
    out.time: total elapsed time (including pre-calculations)

    """
    
    start_time = time.time()
    # Required parameters
    Kernel = torch.tensor(params["K"], dtype=torch.float32)
    phase = torch.tensor(params["input"], dtype=torch.float64)
    
    alpha1 = torch.tensor(params.get("alpha", 1E-5), dtype=torch.float32)
    alpha0 = params.get("alpha0", 2 * alpha1).clone().detach().requires_grad_(False).to(torch.float32)    
    N = phase.shape
    maxOuterIter = params.get("maxOuterIter", 150)
    
    weight = torch.tensor(params.get("weight", np.ones(N)), dtype=torch.float32)
    W = weight * weight
    mu1 = params.get("mu1", 100 * alpha1).clone().detach().requires_grad_(False).to(torch.float32)
    mu0 = params.get("mu0", 200 * alpha1).clone().detach().requires_grad_(False).to(torch.float32)
    mu2 = torch.tensor(params.get("mu2", 1), dtype=torch.float32)
    muH = torch.tensor(params.get("muH", 5), dtype=torch.float32)
    beta = torch.tensor(params.get("beta", 150), dtype=torch.float32)
    mask_a = (W > 0).to(torch.int64)
    mask = params.get("mask", mask_a).clone().detach().requires_grad_(False).to(torch.int32)
    
    tolUpdate = torch.tensor(params.get("tolUpdate", 0.1))
    voxelSize = np.array(params.get("voxelSize", [[1,1,1]]))
    regWeight = torch.from_numpy(np.ones(N + (3,)))
    
    isNonlinear = params.get("isNonlinear", False)
    
    if isNonlinear:
        tolDelta = torch.tensor(params.get("tolDelta", 1E-6))
        z2 =  torch.fft.fftn(W * phase / (W + mu2))
    else:
        # Redefine variable for computational efficiency
        FWy = torch.fft.fftn(W * phase / (W + mu2))
        z2 = FWy  # start with something similar to the input phase, weighted to reduce noise
              
    alpha_over_mu = (alpha1 / mu1).clone().detach().requires_grad_(False).to(torch.float32) # for efficiency
    alpha0_over_mu0 = (alpha0 / mu0).clone().detach().requires_grad_(False).to(torch.float32) # for efficiency
    
    # Precompute gradient-related matrices
    k1, k2, k3 = torch.meshgrid(torch.arange(N[0]), torch.arange(N[1]), torch.arange(N[2]), indexing='xy')
    E1 = (1 - torch.exp(2j * torch.pi * k1 / N[0])) / voxelSize[0, 0]
    E2 = (1 - torch.exp(2j * torch.pi * k2 / N[1])) / voxelSize[0, 1]
    E3 = (1 - torch.exp(2j * torch.pi * k3 / N[2])) / voxelSize[0, 2]

    phi_h = torch.zeros(N, dtype=torch.float32)
    
    z_h = torch.zeros(N, dtype=torch.float32)
    s_h = torch.zeros(N, dtype=torch.float32)    

    #z2 = FWy.clone()  # start with something similar to the input phase, weighted to reduce noise
    s2 = torch.zeros(N)
   
    # Allocate memory for first order gradient
    s1_1 = torch.zeros(N)
    s1_2 = torch.zeros(N)
    s1_3 = torch.zeros(N)
    z1_1 = torch.zeros(N)
    z1_2 = torch.zeros(N)
    z1_3 = torch.zeros(N)

    # Allocate memory for symmetrized gradient
    s0_1 = torch.zeros(N)
    s0_2 = torch.zeros(N)
    s0_3 = torch.zeros(N)
    s0_4 = torch.zeros(N)
    s0_5 = torch.zeros(N)
    s0_6 = torch.zeros(N)
    z0_1 = torch.zeros(N)
    z0_2 = torch.zeros(N)
    z0_3 = torch.zeros(N)
    z0_4 = torch.zeros(N)
    z0_5 = torch.zeros(N)
    z0_6 = torch.zeros(N)

    x = torch.zeros(N)
    zero = torch.tensor(0)
    
    # Move variables to GPU if available  
    torch.cuda.empty_cache()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    if isNonlinear:
        phase = phase.to(device)
    else:
        FWy = FWy.to(device)
    
    z1_1 = z1_1.to(device)
    z1_2 = z1_2.to(device)
    z1_3 = z1_3.to(device)
    
    z0_1 = z0_1.to(device)
    z0_2 = z0_2.to(device)
    z0_3 = z0_3.to(device)
    z0_4 = z0_4.to(device)
    z0_5 = z0_5.to(device)
    z0_6 = z0_6.to(device)
    
    s1_1 = s1_1.to(device)
    s1_2 = s1_2.to(device)
    s1_3 = s1_3.to(device)
    
    s0_1 = s0_1.to(device)
    s0_2 = s0_2.to(device)
    s0_3 = s0_3.to(device)
    s0_4 = s0_4.to(device)
    s0_5 = s0_5.to(device)
    s0_6 = s0_6.to(device)    
    
    x = x.to(device)
    Kernel = Kernel.to(device)
    
    z2 = z2.to(device)
    s2 = s2.to(device)
    
    tolUpdate = tolUpdate.to(device)
    
    E1 = E1.to(device)
    E2 = E2.to(device)
    E3 = E3.to(device)
    
    alpha_over_mu = alpha_over_mu.to(device)
    alpha0_over_mu0 = alpha0_over_mu0.to(device)
    regWeight = regWeight.to(device)
    mu1 = mu1.to(device)
    mu2 = mu2.to(device)
    mu0 = mu0.to(device)
    W = W.to(device)
    alpha1 = alpha1.to(device)
    alpha0 = alpha0.to(device)
    zero = zero.to(device)
    
    phi_h = phi_h.to(device)
    z_h = z_h.to(device)
    s_h = s_h.to(device)
    muH = muH.to(device)
    beta = beta.to(device)
    mask = mask.to(device)
    
    Et1, Et2, Et3 = torch.conj(E1), torch.conj(E2), torch.conj(E3)
    Kt = torch.conj(Kernel)
    
    E1tE1 = Et1 * E1
    E2tE2 = Et2 * E2
    E3tE3 = Et3 * E3
    EE2 = E1tE1 + E2tE2 + E3tE3
    mu0_over_2_E1tE2 = mu0 / 2 * Et1 * E2
    mu0_over_2_E1tE3 = mu0 / 2 * Et1 * E3
    mu0_over_2_E2tE3 = mu0 / 2 * Et2 * E3
    
    KtK_mu1_E_sos = mu2 * Kt * Kernel + mu1 * (E1tE1 + E2tE2 + E3tE3)
    mu1I_mu0_E_wsos1 = mu1 + mu0 * (E1tE1 + (E2tE2 + E3tE3) / 2)
    mu1I_mu0_E_wsos2 = mu1 + mu0 * (E1tE1 / 2 + E2tE2 + E3tE3 / 2)
    mu1I_mu0_E_wsos3 = mu1 + mu0 * ((E1tE1 + E2tE2) / 2 + E3tE3)

    del E1tE1  
    del E2tE2
    del E3tE3
    
    # Precomputation for Cramer's Rule
    a1 = KtK_mu1_E_sos
    a2 = mu1I_mu0_E_wsos1
    a3 = mu1I_mu0_E_wsos2
    a4 = mu1I_mu0_E_wsos3
    a5 = -mu1 * E1
    a6 = -mu1 * E2
    a7 = mu0_over_2_E1tE2
    a8 = -mu1 * E3
    a9 = mu0_over_2_E1tE3
    a10 = mu0_over_2_E2tE3
    
    a5t = torch.conj(a5)
    a6t = torch.conj(a6)
    a7t = torch.conj(a7)
    a8t = torch.conj(a8)
    a9t = torch.conj(a9)
    a10t = torch.conj(a10)
    
    # Free GPU memory for specific variables
    del KtK_mu1_E_sos
    del mu1I_mu0_E_wsos1
    del mu1I_mu0_E_wsos2
    del mu1I_mu0_E_wsos3
    
    # For x
    D11 = a2 * a3 * a4 + a7t * a9 * a10t + a7 * a9t * a10 - a3 * a9 * a9t - a2 * a10 * a10t - a4 * a7 * a7t
    D21 = a3 * a4 * a5t + a6t * a9 * a10t + a7 * a8t * a10 - a3 * a8t * a9 - a5t * a10 * a10t - a4 * a6t * a7
    D31 = a4 * a5t * a7t + a6t * a9 * a9t + a2 * a8t * a10 - a7t * a8t * a9 - a5t * a9t * a10 - a2 * a4 * a6t
    D41 = a5t * a7t * a10t + a6t * a7 * a9t + a2 * a3 * a8t - a7 * a7t * a8t - a3 * a5t * a9t - a2 * a6t * a10t

    # For vx
    D12 = a3 * a4 * a5 + a7t * a8 * a10t + a6 * a9t * a10 - a3 * a8 * a9t - a5 * a10 * a10t - a4 * a6 * a7t
    D22 = a1 * a3 * a4 + a6t * a8 * a10t + a6 * a8t * a10 - a3 * a8 * a8t - a1 * a10 * a10t - a4 * a6 * a6t
    D32 = a1 * a4 * a7t + a6t * a8 * a9t + a5 * a8t * a10 - a7t * a8 * a8t - a1 * a9t * a10 - a4 * a5 * a6t
    D42 = a1 * a7t * a10t + a6 * a6t * a9t + a3 * a5 * a8t - a6 * a7t * a8t - a1 * a3 * a9t - a5 * a6t * a10t

    # For vy
    D13 = a4 * a5 * a7 + a2 * a8 * a10t + a6 * a9 * a9t - a7 * a8 * a9t - a5 * a9 * a10t - a2 * a4 * a6
    D23 = a1 * a4 * a7 + a5t * a8 * a10t + a6 * a8t * a9 - a7 * a8 * a8t - a1 * a9 * a10t - a4 * a5t * a6
    D33 = a1 * a2 * a4 + a5t * a8 * a9t + a5 * a8t * a9 - a2 * a8 * a8t - a1 * a9 * a9t - a4 * a5 * a5t
    D43 = a1 * a2 * a10t + a5t * a6 * a9t + a5 * a7 * a8t - a2 * a6 * a8t - a1 * a7 * a9t - a5 * a5t * a10t

    # For vz
    D14 = a5 * a7 * a10 + a2 * a3 * a8 + a6 * a7t * a9 - a7 * a7t * a8 - a3 * a5 * a9 - a2 * a6 * a10
    D24 = a1 * a7 * a10 + a3 * a5t * a8 + a6 * a6t * a9 - a6t * a7 * a8 - a1 * a3 * a9 - a5t * a6 * a10
    D34 = a1 * a2 * a10 + a5t * a7t * a8 + a5 * a6t * a9 - a2 * a6t * a8 - a1 * a7t * a9 - a5 * a5t * a10
    D44 = a1 * a2 * a3 + a5t * a6 * a7t + a5 * a6t * a7 - a2 * a6 * a6t - a1 * a7 * a7t - a3 * a5 * a5t

    det_A = a1 * D11 - a5 * D21 + a6 * D31 - a8 * D41
    det_Ainv = 1 / (torch.finfo(torch.float64).eps + det_A)
    
    # Free GPU memory for specific variables
    del mu0_over_2_E1tE2
    del mu0_over_2_E1tE3
    del mu0_over_2_E2tE3
    del a1
    del a2
    del a3
    del a4
    del a5
    del a6
    del a7
    del a8
    del a9
    del a10
    del a6t
    del a7t
    del a8t
    del a9t
    del a10t
    del det_A
    
    # You can clear additional variables as needed
    # del D11, D21, D31, D41, D12, D22, D32, D42, D13, D23, D33, D43, D14, D24, D34, D44
    
    reg_time = time.time()
    
    for t in range(maxOuterIter):
        x_prev = x
        # Update x and v: susceptibility estimate
        F_z0_minus_s0_1 = torch.fft.fftn(z0_1 - s0_1)
        F_z0_minus_s0_2 = torch.fft.fftn(z0_2 - s0_2)
        F_z0_minus_s0_3 = torch.fft.fftn(z0_3 - s0_3)
        F_z0_minus_s0_4 = torch.fft.fftn(z0_4 - s0_4)
        F_z0_minus_s0_5 = torch.fft.fftn(z0_5 - s0_5)
        F_z0_minus_s0_6 = torch.fft.fftn(z0_6 - s0_6)
    
        F_z1_minus_s1_1 = torch.fft.fftn(z1_1 - s1_1)
        F_z1_minus_s1_2 = torch.fft.fftn(z1_2 - s1_2)
        F_z1_minus_s1_3 = torch.fft.fftn(z1_3 - s1_3)
        
        mu2_Kt = mu2 * Kt
        Et1_F_z1_minus_s1_1 = Et1 * F_z1_minus_s1_1
        Et2_F_z1_minus_s1_2 = Et2 * F_z1_minus_s1_2
        Et3_F_z1_minus_s1_3 = Et3 * F_z1_minus_s1_3
        
        rhs1 = mu2_Kt * (z2 - s2 - fft.fftn(phi_h)) + mu1 * (Et1_F_z1_minus_s1_1 + Et2_F_z1_minus_s1_2 + Et3_F_z1_minus_s1_3)
        rhs2 = -mu1 * F_z1_minus_s1_1 + mu0 * (Et1 * F_z0_minus_s0_1 + Et2 * F_z0_minus_s0_4 + Et3 * F_z0_minus_s0_5)
        rhs3 = -mu1 * F_z1_minus_s1_2 + mu0 * (Et2 * F_z0_minus_s0_2 + Et1 * F_z0_minus_s0_4 + Et3 * F_z0_minus_s0_6)
        rhs4 = -mu1 * F_z1_minus_s1_3 + mu0 * (Et3 * F_z0_minus_s0_3 + Et1 * F_z0_minus_s0_5 + Et2 * F_z0_minus_s0_6)
            
        # Free GPU memory for specific variables
        del F_z0_minus_s0_1
        del F_z0_minus_s0_2
        del F_z0_minus_s0_3
        del F_z0_minus_s0_4
        del F_z0_minus_s0_5
        del F_z0_minus_s0_6
        del F_z1_minus_s1_1
        del F_z1_minus_s1_2
        del F_z1_minus_s1_3

        # Cramer's rul
        Fx = (rhs1 * D11 - rhs2 * D21 + rhs3 * D31 - rhs4 * D41) * det_Ainv
        Fv1 = (-rhs1 * D12 + rhs2 * D22 - rhs3 * D32 + rhs4 * D42) * det_Ainv
        Fv2 = (rhs1 * D13 - rhs2 * D23 + rhs3 * D33 - rhs4 * D43) * det_Ainv
        Fv3 = (-rhs1 * D14 + rhs2 * D24 - rhs3 * D34 + rhs4 * D44) * det_Ainv
            
        # Free GPU memory for specific variables
        del rhs1
        del rhs2
        del rhs3
        del rhs4
            
        x = mask * torch.real(torch.fft.ifftn(Fx))
        Fx = torch.fft.fftn(x)  # Extra step to increase stability from the proximal step
        v1 = torch.real(torch.fft.ifftn(Fv1))
        v2 = torch.real(torch.fft.ifftn(Fv2))
        v3 = torch.real(torch.fft.ifftn(Fv3))
    
        x_update = 100 * torch.norm(x - x_prev) / torch.norm(x)
        print(f'Iter: {t+1}   Update: {x_update:.6f}')
          
        if x_update < tolUpdate:
            print("\nEarly stopping reached.")
            break
        
        if t < maxOuterIter:
        
            # Compute gradients for z0 and z1 update
            Dx1 = torch.real(torch.fft.ifftn(E1 * Fx))
            Dx2 = torch.real(torch.fft.ifftn(E2 * Fx))
            Dx3 = torch.real(torch.fft.ifftn(E3 * Fx))
        
            E_v1 = torch.real(torch.fft.ifftn(E1 * Fv1))
            E_v2 = torch.real(torch.fft.ifftn(E2 * Fv2))
            E_v3 = torch.real(torch.fft.ifftn(E3 * Fv3))
            E_v4 = torch.real(torch.fft.ifftn(E1 * Fv2 + E2 * Fv1)) / 2
            E_v5 = torch.real(torch.fft.ifftn(E1 * Fv3 + E3 * Fv1)) / 2
            E_v6 = torch.real(torch.fft.ifftn(E2 * Fv3 + E3 * Fv2)) / 2
            
            # Free GPU memory for specific variables
            del Fv1
            del Fv2
            del Fv3
            
            # Update z0: Symm grad
            z0_1 = torch.max(torch.abs(E_v1 + s0_1) - alpha0_over_mu0, zero) * torch.sign(E_v1 + s0_1)
            z0_2 = torch.max(torch.abs(E_v2 + s0_2) - alpha0_over_mu0, zero) * torch.sign(E_v2 + s0_2)
            z0_3 = torch.max(torch.abs(E_v3 + s0_3) - alpha0_over_mu0, zero) * torch.sign(E_v3 + s0_3)
            z0_4 = torch.max(torch.abs(E_v4 + s0_4) - alpha0_over_mu0, zero) * torch.sign(E_v4 + s0_4)
            z0_5 = torch.max(torch.abs(E_v5 + s0_5) - alpha0_over_mu0, zero) * torch.sign(E_v5 + s0_5)
            z0_6 = torch.max(torch.abs(E_v6 + s0_6) - alpha0_over_mu0, zero) * torch.sign(E_v6 + s0_6)
                
            # Update z1: Grad
            z1_1 = torch.max(torch.abs(Dx1 - v1 + s1_1) - regWeight[..., 0] * alpha_over_mu, zero) * torch.sign(Dx1 - v1 + s1_1)
            z1_2 = torch.max(torch.abs(Dx2 - v2 + s1_2) - regWeight[..., 1] * alpha_over_mu, zero) * torch.sign(Dx2 - v2 + s1_2)
            z1_3 = torch.max(torch.abs(Dx3 - v3 + s1_3) - regWeight[..., 2] * alpha_over_mu, zero) * torch.sign(Dx3 - v3 + s1_3)
            
            # Update z2 and s2: data consistency
            rhs_z2 = mu2 * torch.real(fft.ifftn(Kernel * Fx + s2 + phi_h))
            
            if isNonlinear:
                z2 = rhs_z2 / mu2
                    
                # Newton-Raphson method
                delta = float('inf')
                inn = 0
                while (delta > tolDelta and inn < 50):
                    inn = inn + 1
                    norm_old = torch.norm(z2.reshape(-1))
                
                    update = (W * torch.sin(z2 - phase) + mu2 * z2 - rhs_z2) / (W * torch.cos(z2 - phase) + mu2)
            
                    z2 -= update
                    delta = torch.norm(update.reshape(-1)) / norm_old
                z2 = fft.fftn(z2)    
                # Free GPU memory for specific variables
                del update
            else:
                z2 = FWy + torch.fft.fftn(rhs_z2 / (W + mu2))
                
            Fphi_h = (muH * torch.conj(EE2) * fft.fftn(z_h - s_h) + mu2 * (z2 - s2) - mu2 * Kernel * Fx) / (torch.finfo(torch.float64).eps + mu2 + muH * EE2 * torch.conj(EE2))
            phi_h = torch.real(fft.ifftn(Fphi_h))
            z_h = muH * (torch.real(fft.ifftn(EE2 * Fphi_h)) + s_h) / (muH + beta * mask)
            s2 = s2 + Kernel * Fx - z2 + fft.fftn(phi_h)
                
            # Update s0 and s1
            s0_1 = s0_1 + E_v1 - z0_1
            s0_2 = s0_2 + E_v2 - z0_2
            s0_3 = s0_3 + E_v3 - z0_3
            s0_4 = s0_4 + E_v4 - z0_4
            s0_5 = s0_5 + E_v5 - z0_5
            s0_6 = s0_6 + E_v6 - z0_6
        
            s1_1 = s1_1 + Dx1 - v1 - z1_1
            s1_2 = s1_2 + Dx2 - v2 - z1_2
            s1_3 = s1_3 + Dx3 - v3 - z1_3
            
            # Free GPU memory for specific variables
            del rhs_z2
            del Dx1
            del Dx2
            del Dx3
            del E_v1
            del E_v2
            del E_v3
            del E_v4
            del E_v5
            del E_v6
            
            s_h = s_h + torch.real(fft.ifftn(EE2 * Fphi_h)) - z_h
            
    # Extract output values
    end_time = time.time()
    reg_time = end_time - reg_time
    total_time = end_time - start_time
    print(f'Regularization Time: {reg_time:.4f} seconds')
    print(f'Total Execution Time: {total_time:.4f} seconds')
    
    out = {"x": x.cpu().detach().numpy().astype(np.float32), "iter": t+1, "time": total_time}
    
    return out

