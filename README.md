# FANSI-toolbox-Python

The FAst Nonlinear Susceptibility Inversion (FANSI) Toolbox for Python was made  
by Manuel Sepúlveda at Pontificia Universidad Catolica de Valparaíso, on 2023.  

The code in this toolbox is based on the code released by Carlos Milovic at  
https://gitlab.com/cmilovic/FANSI-toolbox/-/tree/master.  
Milovic C, Bilgic B, Zhao B, Acosta-Cabronero J, Tejos C. Fast Nonlinear  
Susceptibility Inversion with Variational Regularization. Magn Reson Med.  
Accepted Dec. 12th 2017. doi: 10.1002/mrm.27073.  
Bilgic B., Chatnuntawech I., Langkammer C., Setsompop K.; Sparse Methods for  
Quantitative Susceptibility Mapping; Wavelets and Sparsity XVI, SPIE 2015.  

This toolbox provide algorithms that perform the final dipole inversion step in  
the quantitative susceptibility mapping (QSM) pipeline.  
This includes a sample script to show how to use the functions in this  
toolbox, and how to set the principal variables.  
This example uses an analytic susceptibility brain phantom (data phantom  
folder) based on the phantom developed by C Langkammer, et al NeuroImage 2015  
and C. Wisnieff, et al NeuroImage 2014. doi: 10.1016/j.neuroimage.2015.02.041.  
doi: 10.1016/j.neuroimage.2012.12.050.  

## Required libraries

NumPy  
Scipy  
CuPy/PyTorch  
Matplotlib  
Time  

## Modules

The source code for the Fast Nonlinear Susceptibility Inversion (FANSI) method  
for Python is organized into different modules, as described below:  

### FANSI_module

Fast Nonlinear Susceptibility Inversion (FANSI) method based on the variable  
splitting and alternating direction method of multipliers.  
Includes Total Variation and Total Generalized Variation regularization.  
Based on:  
Milovic C, Bilgic B, Zhao B, Acosta-Cabronero J, Tejos C. Fast Nonlinear  
Susceptibility Inversion with Variational Regularization. Magn Reson Med.  
Accepted Dec. 12th 2017. doi: 10.1002/mrm.27073.  

### eNDI_module

Gradient Descent and a Conjugate Gradient method to perform nonregularized  
Nonlinear Dipole Inversions with Early Stopping algorithm.  
Based on:  
Polak et al NMR Biomed 2020. doi:10.1002/nbm.4271.  
Milovic C, Karsa A, Shmueli K. Efficient Early Stopping algorithm for  
Quantitative Susceptibility Mapping (QSM). ESMRMB virtual meeting 2020.  

### L1_module

An L1-norm data fidelity term based QSM method that rejects phase  
inconsistencies, noise and errors, preventing streaking artifacts.  
Based on:  
Milovic C, Lambert M, Langkammer C, Bredies K, Irarrazaval P, and Tejos C.  
Streaking artifact suppression of quantitative susceptibility mapping  
reconstructions via L1-norm data fidelity optimization (L1-QSM) Magn Reson Med.  
2021; 00: 1– 17. doi: 10.1002/mrm.28957.  

### WH_module

Weak harmonic QSM method to jointly reconstruct susceptibility maps and  
background (harmonic) field remnants.  
Based on:  
Milovic C, Bilgic B, Zhao B, Langkammer C, Tejos C and Acosta-Cabronero J.  
Weak-harmonic regularization for quantitative susceptibility mapping (WH-QSM);  
Magn Reson Med, 2019;81:1399-1411.  

You may also use the "help()" commands to see the header of each function and  
learn about all the function parameters.  
